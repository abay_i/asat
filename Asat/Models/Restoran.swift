//
//  Restoran.swift
//  Asat
//
//  Created by Александр on 19.07.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import Foundation
import ObjectMapper
import GoogleMaps

class RestoranLocation: Mappable {
    
    var id: Int?
    var restaurant_id: Int?
    var city_id: Int?
    var name: String?
    var address: String?
    var ts: String?
    var status: Int?
    var price: Int?
    var delivery_time: Int?
    var coords: [[[Double]]] = [] {
        didSet {
            pathOfPolygon = GMSMutablePath()
            for level1 in coords {
                for coords in level1 {
                    let lat = coords[0] 
                    let lon = coords[1] 
                    let coord = CLLocationCoordinate2D(latitude: lat, longitude: lon)
                    pathOfPolygon?.add(coord)
                }
            }
        }
    }
    var pathOfPolygon: GMSMutablePath?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id <- map["id"]
        restaurant_id <- map["restaurant_id"]
        city_id <- map["city_id"]
        name <- map["name"]
        address <- map["address"]
        ts <- map["ts"]
        status <- map["status"]
        coords <- map["coords"]
        price <- map["price"]
        delivery_time <- map["delivery_time"]
        coords <- map["coords"]
    }

    func userInCoords(point: CLLocationCoordinate2D) -> Bool {
        var result = false
       
        if let polygons = pathOfPolygon {
            if (GMSGeometryContainsLocation(point, polygons, true)) {
                result = true
            }
        }

        return result
    }
    
}

class Restoran: NSObject, Mappable, NSCoding {
    
    var id: Int?
    var name: String?
    var user_id: Int?
    var city_id: Int?
    var delivery_type: Int?
    var work_start: String?
    var work_end: String?
    var min_price: Int?
    var kitchens: [Int] = []
    var online_payment: Bool?
    var pickup: Bool?
    var vip_type: Int?
    var logo: String?
    var cash_back: Int?
    var info: ResoranInfo?
    var menu: [RestoransMenu]?
    var locations: [RestoranLocation]?
    var reviews: [Comment]?
    var rating: Int?
    
    var min_price_description: String? {
        get {
            if self.min_price != nil {
                return "\(self.min_price!) тг"
            }
            
            return nil
        }
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(name, forKey: "name")
        aCoder.encode(user_id, forKey: "user_id")
        aCoder.encode(city_id, forKey: "city_id")
        aCoder.encode(delivery_type, forKey: "delivery_type")
        aCoder.encode(work_start, forKey: "work_start")
        aCoder.encode(work_end, forKey: "work_end")
        aCoder.encode(min_price, forKey: "min_price")
        aCoder.encode(online_payment, forKey: "online_payment")
        aCoder.encode(pickup, forKey: "pickup")
        aCoder.encode(vip_type, forKey: "vip_type")
        aCoder.encode(cash_back, forKey: "cash_back")
        aCoder.encode(rating, forKey: "rating")
    }
    
    required init(coder aDecoder: NSCoder) {
        self.id = aDecoder.decodeObject(forKey: "id") as? Int
        self.user_id = aDecoder.decodeObject(forKey: "user_id") as? Int
        self.city_id = aDecoder.decodeObject(forKey: "city_id") as? Int
        self.delivery_type = aDecoder.decodeObject(forKey: "delivery_type") as? Int
        self.min_price = aDecoder.decodeObject(forKey: "min_price") as? Int
        self.pickup = aDecoder.decodeObject(forKey: "pickup") as? Bool
        self.vip_type = aDecoder.decodeObject(forKey: "vip_type") as? Int
        self.online_payment = aDecoder.decodeObject(forKey: "online_payment") as? Bool
        self.name = aDecoder.decodeObject(forKey: "name") as? String
        self.work_start = aDecoder.decodeObject(forKey: "work_start") as? String
        self.work_end = aDecoder.decodeObject(forKey: "work_end") as? String
        self.cash_back = aDecoder.decodeObject(forKey: "cash_back") as? Int
        self.rating = aDecoder.decodeObject(forKey: "rating") as? Int
    }
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        user_id <- map["user_id"]
        city_id <- map["city_id"]
        delivery_type <- map["delivery_type"]
        work_start <- map["work_start"]
        work_end <- map["work_end"]
        min_price <- map["min_price"]
        kitchens <- map["kitchens"]
        online_payment <- map["online_payment"]
        pickup <- map["pickup"]
        vip_type <- map["vip_type"]
        logo <- map["logo"]
        info <- map["info"]
        menu <- map["menu"]
        cash_back <- map["cash_back"]
        locations <- map["locations"]
        reviews <- map["reviews"]
        rating <- map["rating"]
    }
    
}

class ResoranInfo: Mappable {
    
    var id: Int?
    var address: String?
    var photo: String?
    var logo: String?
    var contacts: String?
    var directorFIO: String?
    var directorContacts: String?
    var manager: String?
    var description: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        address <- map["address"]
        photo <- map["photo"]
        logo <- map["logo"]
        contacts <- map["contacts"]
        directorFIO <- map["director_fio"]
        directorContacts <- map["director_contacts"]
        description <- map["description"]
        manager <- map["manager"]
    }
}

class RestoransMenu: NSObject, Mappable, NSCoding {
    
    var id: Int?
    var name: String?
    var price: Int?
    var category_id: Int?
    var restaurant_id: Int?
    var photo: String?
    var desc: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        price <- map["price"]
        category_id <- map["category_id"]
        restaurant_id <- map["restaurant_id"]
        photo <- map["photo"]
        desc <- map["description"]
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(name, forKey: "name")
        aCoder.encode(price, forKey: "price")
        aCoder.encode(category_id, forKey: "category_id")
        aCoder.encode(restaurant_id, forKey: "restaurant_id")
        aCoder.encode(photo, forKey: "photo")
        aCoder.encode(desc, forKey: "desc")
    }
    
    required init(coder aDecoder: NSCoder) {
        self.id = aDecoder.decodeObject(forKey: "id") as? Int
        self.name = aDecoder.decodeObject(forKey: "name") as? String
        self.price = aDecoder.decodeObject(forKey: "price") as? Int
        self.category_id = aDecoder.decodeObject(forKey: "category_id") as? Int
        self.restaurant_id = aDecoder.decodeObject(forKey: "restaurant_id") as? Int
        self.photo = aDecoder.decodeObject(forKey: "photo") as? String
        self.desc = aDecoder.decodeObject(forKey: "desc") as? String
    }
}
