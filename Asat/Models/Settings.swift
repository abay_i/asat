//
//  Settings.swift
//  Asat
//
//  Created by Александр on 09.10.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import Foundation


class Settings: NSObject, NSCoding {
    var lang: String? = "ru"
    var usePush: Bool? = true
    var useImage: Bool? = true
//    var payment: Int? = 1
    
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(lang, forKey: "lang")
        aCoder.encode(usePush, forKey: "usePush")
        aCoder.encode(useImage, forKey: "useImage")
//        aCoder.encode(payment, forKey: "payment")
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.lang = aDecoder.decodeObject(forKey: "lang") as? String
        self.usePush = aDecoder.decodeObject(forKey: "usePush") as? Bool
        self.useImage = aDecoder.decodeObject(forKey: "useImage") as? Bool
//        self.payment = aDecoder.decodeObject(forKey: "payment") as? Int
    }
    
    override init() {
        super.init()
    }
}
