//
//  Order.swift
//  Asat
//
//  Created by Александр on 24.08.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import Foundation
import ObjectMapper

class Order: Mappable {
    
    var id: Int?
    var payment_type: Int?
    var user_id: Int?
    var restaurant_id: Int?
    var preorder: String?
    var persons: Int?
    var comment: String?
    var change: Int?
    var type: Int?
    var status: Int?
    var price: Int?
    var restaurant: Restoran?
    var fio: String?
    var address: String?
    var items: [OrderItem] = []
    var ts: String?
    var paid: Bool?
    
    var statusDesc: String {
        get {
            if let status = self.status {
                switch status {
                case 0:
                    return "Новый"
                case 1:
                    return "В обработке"
                case 2:
                    return "В пути"
                case 3:
                    return "Доставлен"
                case 4:
                    return "Отменен"
                default:
                    return ""
                }
            } else {
                return ""
            }
        }
    }
    
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        payment_type <- map["payment_type"]
        user_id <- map["user_id"]
        restaurant_id <- map["restaurant_id"]
        preorder <- map["preorder"]
        persons <- map["persons"]
        comment <- map["comment"]
        change <- map["comment"]
        type <- map["type"]
        status <- map["status"]
        price <- map["price"]
        id <- map["id"]
        restaurant <- map["restaurant"]
        fio <- map["fio"]
        address <- map["address"]
        items <- map["items"]
        ts <- map["ts"]
        paid <- map["is_paid"]
        
    }
    
    func getUniqItems() -> [OrderItem] {
        var uniqItems: [OrderItem] = []
        
        for item in items {
            
            let contain = uniqItems.contains(where: { (i) -> Bool in
                return i.product_id == item.product_id
            })
            
            if contain {
                continue
            }
            
            let orders = items.filter{ $0.product_id == item.product_id }
            item.count = orders.count
            uniqItems.append(item)
        }
        
        return uniqItems
    }
}

class OrderItem: Mappable {
    
    var id: Int?
    var order_id: Int?
    var product_id: Int?
    var price: Int?
    var ts: String?
    var product: RestoransMenu?
    
    var count: Int = 1
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id <- map["id"]
        order_id <- map["order_id"]
        product_id <- map["product_id"]
        price <- map["price"]
        ts <- map["ts"]
        product <- map["product"]
    }

}
