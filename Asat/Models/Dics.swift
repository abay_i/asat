//
//  Dics.swift
//  Asat
//
//  Created by Александр on 19.09.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import Foundation
import ObjectMapper

class City: Mappable {
    var id: Int?
    var name: String?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id <- map["id"]
        
        //TODO: JSON
        name <- map["name"]
    }
}


class Kitchen: Mappable {
    var id: Int?
    var name: String?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
    }
}
