//
//  User.swift
//  Asat
//
//  Created by Александр on 14.08.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import Foundation
import ObjectMapper

class User: Mappable {
    
    enum Role: String {
        case client = "client"
        case restaurant = "restaurant"
        case courier = "courier"
        case none
    }
    
    var id: Int?
    var email: String?
    var phone: String?
    var photo: String?
    var fio: String?
    var is_online: Int?
    var roleString: String?
    var userInfo: UserInfo?
    var balance: Double?
    
    var role: Role {
        get {
            if let _ = roleString {
                return Role.init(rawValue: roleString!)!
            }
            
            return Role.none
        }
    }
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id <- map["id"]
        email <- map["email"]
        phone <- map["phone"]
        photo <- map["photo"]
        fio <- map["fio"]
        is_online <- map["is_online"]
        roleString <- map["role"]
        userInfo <- map["person"]
        balance <- map["balance"]
    }
}


class UserInfo: NSObject, Mappable, NSCoding {
    
    var id: Int?
    var user_id: Int?
    var city_id: Int?
    var address: String?
    var flat: String?
    var entrance: String?
    var floor: Int?
    var intercom: String?
    var ts: String?
    var is_deleted: Int?
    var info: String?
    
    var geoValid: Bool {
        get {
            return ((city_id != nil) && (address != nil))
        }
    }
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id <- map["id"]
        user_id <- map["user_id"]
        city_id <- map["city_id"]
        address <- map["address"]
        flat <- map["flat"]
        entrance <- map["entrance"]
        intercom <- map["intercom"]
        ts <- map["ts"]
        is_deleted <- map["is_deleted"]
        info <- map["info"]
        floor <- map["floor"]
    }
    
    static func ==(f: UserInfo, s: UserInfo) -> Bool {
        return (f.city_id == s.city_id) &&
                (f.address == s.address) &&
                (f.flat == s.flat) &&
                (f.floor == s.floor) &&
                (f.city_id == s.city_id)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(user_id, forKey: "user_id")
        aCoder.encode(city_id, forKey: "city_id")
        aCoder.encode(address, forKey: "address")
        aCoder.encode(flat, forKey: "flat")
        aCoder.encode(entrance, forKey: "entrance")
        aCoder.encode(floor, forKey: "floor")
        aCoder.encode(intercom, forKey: "intercom")
        aCoder.encode(ts, forKey: "ts")
        aCoder.encode(is_deleted, forKey: "is_deleted")
        aCoder.encode(info, forKey: "info")
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.id = aDecoder.decodeObject(forKey: "id") as? Int
        self.user_id = aDecoder.decodeObject(forKey: "user_id") as? Int
        self.city_id = aDecoder.decodeObject(forKey: "city_id") as? Int
        self.address = aDecoder.decodeObject(forKey: "address") as? String
        self.flat = aDecoder.decodeObject(forKey: "flat") as? String
        self.entrance = aDecoder.decodeObject(forKey: "entrance") as? String
        self.floor = aDecoder.decodeObject(forKey: "floor") as? Int
        self.intercom = aDecoder.decodeObject(forKey: "intercom") as? String
        self.ts = aDecoder.decodeObject(forKey: "ts") as? String
        self.is_deleted = aDecoder.decodeObject(forKey: "is_deleted") as? Int
        self.info = aDecoder.decodeObject(forKey: "info") as? String
    }
    
    override init() {
        super.init()
    }
    
}
