//
//  Comment.swift
//  Asat
//
//  Created by Александр on 29.08.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import Foundation
import ObjectMapper

class Comment: Mappable {
    
    var id: Int?
    var restaurant_id: Int?
    var user_id: Int?
    var rating: Int?
    var comment: String?
    var ts: String?
    var user: User?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id <- map["id"]
        restaurant_id <- map["restaurant_id"]
        user_id <- map["user_id"]
        rating <- map["rating"]
        comment <- map["comment"]
        ts <- map["ts"]
        user <- map["user"]
    }
}
