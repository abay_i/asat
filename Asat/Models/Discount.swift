//
//  Discount.swift
//  Asat
//
//  Created by Александр on 12.11.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import Foundation
import ObjectMapper

class Discount: Mappable {
    
    var id: Int?
    var restaurant_id: Int?
    var name: String?
    var description: String?
    var photo: String?
    var amount: Int?
    var status: Int?
    var is_deleted: Int?
    var ts: String?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id <- map["id"]
        restaurant_id <- map["restaurant_id"]
        name <- map["name"]
        description <- map["description"]
        photo <- map["photo"]
        amount <- map["amount"]
        status <- map["status"]
        is_deleted <- map["is_deleted"]
        ts <- map["ts"]
    }
}
