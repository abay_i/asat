//
//  Basket.swift
//  Asat
//
//  Created by Александр on 16.08.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import Foundation

class Basket: NSObject, NSCoding {
    func encode(with aCoder: NSCoder) {
        aCoder.encode(food, forKey: "food")
        aCoder.encode(count, forKey: "count")
    }

    
    var food: RestoransMenu
    var count: Int
    
    init(food: RestoransMenu, count: Int) {
        self.food = food
        self.count = count
    }
    
    required init(coder aDecoder: NSCoder) {
        self.food = aDecoder.decodeObject(forKey: "food") as! RestoransMenu
        self.count = aDecoder.decodeInteger(forKey: "count")
    }
    
    func encodeWithCoder(aCoder: NSCoder!) {
        aCoder.encode(food, forKey: "food")
        aCoder.encode(count, forKey: "count")
    }
}

class BasketRestoran: NSObject, NSCoding {
    var restoran: Restoran
    var foods: [Basket] = []
    
    var allPrice: Int {
        get {
            var price = 0
            
            for item in foods {
                price += (item.food.price! * item.count)
            }
            
            return price
        }
    }
    
    init(restoran: Restoran) {
        self.restoran = restoran
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(restoran, forKey: "restoran")
        aCoder.encode(foods, forKey: "foods")
    }
    
    required init(coder aDecoder: NSCoder) {
        self.restoran = aDecoder.decodeObject(forKey: "restoran") as! Restoran
        self.foods = aDecoder.decodeObject(forKey: "foods") as! [Basket]
    }
    
}

class BasketList {
    static var data: [BasketRestoran] = []
    
    
    init() {
        BasketList.data = UserDefaults.basket
    }
    
    static func initBasket() {
        BasketList.data = UserDefaults.basket
    }
    
    static func remove(by id: Int, important: Bool = false) {
        let basket = self.findIndex(by: id)
        if basket != nil {
            BasketList.data[basket!.index.section].foods.remove(at: basket!.index.row)
            if BasketList.data[basket!.index.section].foods.count == 0 || important {
                BasketList.data.remove(at: basket!.index.section)
                UserDefaults.basket = BasketList.data
            }
        }
    }
    
    static func changeOrAdd(restoran: Restoran, food: RestoransMenu, count: Int) {
        
        let basket = findIndex(by: food.id!)
        
        guard count > 0 else {
            if basket != nil {
                BasketList.remove(by: food.id!)
                UserDefaults.basket = BasketList.data
            }
            
            return
        }
        
        if let issetBasket = basket {
            BasketList.data[issetBasket.index.section].foods[issetBasket.index.row].count = count
        } else {
            let basket = Basket(food: food, count: count)
            
            if let issetRestoran = BasketList.findRestoranById(restoran.id!) {
                BasketList.data[issetRestoran.section].foods.append(basket)
                
            } else {
                let basketRestoran = BasketRestoran(restoran: restoran)
                basketRestoran.foods.append(basket)
                BasketList.data.append(basketRestoran)
            }
        }
        
        UserDefaults.basket = BasketList.data
    }
    
   static func findIndex(by id: Int) -> (index: IndexPath, basket: Basket)? {

        for (section, item) in data.enumerated() {
            for (row, basket) in item.foods.enumerated() {
                if basket.food.id == id {
                    let indexPath = IndexPath(row: row, section: section)
                    return (indexPath, basket)
                }
            }
        }
    
        return nil
    }
    
    static func findRestoranById(_ id: Int) -> (restoran: Restoran, section: Int)? {
        
        for (section, item) in data.enumerated() {
            if item.restoran.id == id {
                return (item.restoran, section)
            }
        }
        
        return nil
    }
    
    static func getAllPrice() -> Int {
        var price = 0
        
        for basketRestoran in BasketList.data {
            for item in basketRestoran.foods {
                let foodsPrice = item.food.price! * item.count
                price += foodsPrice
            }
            
        }
        
        return price
    }
    
    static func generateCart(_ restoran_id: Int? = nil) -> [[String: Any]] {
        var cart: [[String: Any]] = []
        
        for item in data {
            if item.restoran.id != restoran_id {
                continue
            }
            let restoranId = item.restoran.id!
            var basket: [String: Int] = [:]
            
            for food in item.foods {
                basket[food.food.id!.description] = food.count
            }
            
            var restoranCart: [String: Any] = [:]
            
            restoranCart["restoran_id"] = restoranId
            restoranCart["cart"] = basket
            
            cart.append(restoranCart)
        }
        
        return cart
    }
    
        
}
