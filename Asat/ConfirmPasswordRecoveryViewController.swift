//
//  ConfirmPasswordRecoveryViewController.swift
//  Asat
//
//  Created by Abay on 7/6/18.
//  Copyright © 2018 Александр. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class ConfirmPasswordRecoveryViewController: ViewController {

    var userId: String!
    var phoneNumber: String!
    
    @IBOutlet weak var codeTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordTextField: SkyFloatingLabelTextField!
    
    @IBAction func sendAction(_ sender: UIButton) {
        
        guard !codeTextField.text!.isEmpty else {
            alertValidation(message: "Необходимо заполнить «Код»", title: "Ошибка")
            return
        }
        
        guard !passwordTextField.text!.isEmpty else {
            alertValidation(message: "Необходимо заполнить «Пароль»", title: "Ошибка")
            return
        }
        
        let hud = showProgressHud()
        API.verifyRecoverPhone(userId: userId, code: codeTextField.text!, newPassword: passwordTextField.text!) { [weak self] data, errors, code in
            guard let strongSelf = self else { return }
            
            strongSelf.hideProgressHud(hud)
            
            if let error = errors {
                print(error)
                strongSelf.alertValidation(message: error, title: "Не удалось восстановить пароль")
            } else {
                print(data)
                if code == 201 {
                    if let token = data["token"].string {
                        UserDefaults.token = token
                        let authHud = strongSelf.showProgressHud()
                        API.getUser(compliteHandler: { [weak self] (data, error, code) -> Void in
                            strongSelf.hideProgressHud(authHud)
                            if let user = data {
                                (UIApplication.shared.delegate as! AppDelegate).user = user
                                PushManager.updateFCMToken()
                                if let info = user.userInfo {
                                    UserDefaults.userInfo = info
                                    strongSelf.present(MainMenuViewController(), animated: true, completion: nil)
                                } else if user.role == .client {
                                    if let controller = self?.storyboard?.instantiateViewController(withIdentifier: "UserLocationMenuViewController") {
                                        strongSelf.navigationController?.pushViewController(controller, animated: true)
                                    }
                                } else {
                                    strongSelf.present(MainMenuViewController(), animated: true, completion: nil)
                                }
                                
                            } else {
                                (UIApplication.shared.delegate as! AppDelegate).unsetUser()
                                print(error ?? "authorization error")
                                strongSelf.alertValidation(message: "Что то пошло не так", title: "Ошибка")
                            }
                        })
                    } else {
                        strongSelf.alertValidation(message: "Что то пошло не так", title: "Ошибка")
                    }
                } else {
                    strongSelf.alertValidation(message: "Что то пошло не так", title: "Ошибка")
                }
            }
        }
    }
    
    @IBAction func requestCodeAction(_ sender: Any) {
        let hud = showProgressHud()
        API.recoverPhone(phone: phoneNumber, compliteHandler: { [weak self] (data, errors, code) -> Void in
            if let error = errors {
                print(error)
                self?.alertValidation(message: error, title: "Не удалось получить код")
            }
            self?.hideProgressHud(hud)
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        codeTextField.delegate = self
        passwordTextField.delegate = self
    }

}

extension ConfirmPasswordRecoveryViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
}
