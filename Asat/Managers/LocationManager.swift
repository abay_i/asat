//
//  LocationManager.swift
//  Asat
//
//  Created by Dmitriy Safarov on 16/11/2017.
//  Copyright © 2017 Александр. All rights reserved.
//

import Foundation
import CoreLocation
import SwiftLocation

class SLocationManager {
    
    // MARK: - Singleton
    
    static let sharedInstance = SLocationManager()
    
    // MARK: - Variables

    private var continuousRequest: LocationRequest?
    
    // MARK: - Public Methods
    
    public func getCurrentCoordinate(recievedCoordinate: @escaping (CLLocationCoordinate2D) -> Void, onError: @escaping (Error?) -> Void) {
        Locator.currentPosition(accuracy: .house, onSuccess: { location in
            recievedCoordinate(location.coordinate)
        }) { (error, last) in
            onError(error)
            print("Failed to get location: \(String(describing: error))")
        }
    }
    
    public func startMonitoringLocations(recievedCoordinate: @escaping (CLLocationCoordinate2D) -> Void, onError: @escaping (Error?) -> Void) {
        
        continuousRequest = Locator.subscribePosition(accuracy: .house, onUpdate: { (location) in
            recievedCoordinate(location.coordinate)
        }) {[weak self] (error, last) in
            self?.continuousRequest?.stop()
            onError(error)
        }
    }
    
    public func stopMonitoringLocations() {
            continuousRequest?.stop()
    }
    
    // MARK: - Private Methods
    
}


















