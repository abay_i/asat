//
//  ViewController.swift
//  Emenu
//
//  Created by Александр on 16.06.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var shouldMoveToLogin = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let navigationBackButton = UIBarButtonItem(title: "Назад", style: .plain, target: self, action: #selector(self.pop))
        self.navigationItem.backBarButtonItem = navigationBackButton
        internalViewDidLoad()
    }
    
    public func internalViewDidLoad() {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)        
        UIView.animate(withDuration: 0.5, animations: {
            self.subviewsHidden(hidden: false)
        }) { [weak self] _ in
            guard let strongSelf = self else { return }
            if strongSelf.shouldMoveToLogin {
                strongSelf.shouldMoveToLogin = false
                strongSelf.subviewsHidden(hidden: true)
                let controller = strongSelf.storyboard?.instantiateViewController(withIdentifier: "AuthController") as! AuthViewController
                strongSelf.navigationController?.pushViewController(controller, animated: true)
            }
        }
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        subviewsHidden(hidden: true)
    }
    
    public func errorAlertRequest() {
        
    }
    
    public func navigationSetup() {
        
    }
    
    @objc func pop() {
        self.navigationController?.popViewController(animated: true)
    }
    
    public func subviewsHidden(hidden: Bool) {
        let views = view.subviews
        
        for view in views {
            view.alpha = !hidden ? 1 : 0
            view.isHidden = hidden
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        subviewsHidden(hidden: true)
    }
    

}



