//
//  View.swift
//  Emenu
//
//  Created by Александр on 18.06.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit
import Cosmos

class RestoransListCell: UITableViewCell {
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var restoranImageView: UIImageView!
    @IBOutlet weak var restoranTitleLabel: UILabel!
    @IBOutlet weak var cashbackLabel: UILabel!
    @IBOutlet weak var minPriceLabel: UILabel!
    @IBOutlet weak var deliveryTimeLabel: UILabel!
    @IBOutlet weak var deliveryPriceLabel: UILabel!
    @IBOutlet weak var workTimeLabel: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var deliveryYesNoImageView: UIImageView!
    @IBOutlet weak var halalYesNoImageView: UIImageView!
    @IBOutlet weak var cardYesNoImageView: UIImageView!
  
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        if selected {
            backView.backgroundColor = .red
        }
        else {
            backView.backgroundColor = .white
        }
    }
    




}
