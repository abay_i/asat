//
//  RestoranMenuTableViewCell.swift
//  Asat
//
//  Created by Александр on 14.08.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit

protocol RestoranMenuTableViewCellProtocol {
    func counterWasChanged(indexPath: IndexPath, count: Int)
}

class RestoranMenuTableViewCell: UITableViewCell {
    
    @IBOutlet weak var foodImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var counterView: CounterView!
    
    var indexPath: IndexPath!
    var delegate: RestoranMenuTableViewCellProtocol?

    override func awakeFromNib() {
        super.awakeFromNib()
        counterView.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        //super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension RestoranMenuTableViewCell: CounterProtocol {
    func afterChangeValue(value: Int) {
        delegate?.counterWasChanged(indexPath: indexPath, count: value)
    }
}


