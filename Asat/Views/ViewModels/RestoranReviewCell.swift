//
//  RestoranReviewCell.swift
//  Asat
//
//  Created by Александр on 28.08.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit

class RestoranReviewCell: UITableViewCell {

    @IBOutlet weak var avatarImageView: RoundedImageView!
    @IBOutlet weak var fioLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!

}
