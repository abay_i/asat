//
//  RoundedImageView.swift
//  Asat
//
//  Created by Александр on 28.08.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit

@IBDesignable
class RoundedImageView: UIImageView {
    
    override func prepareForInterfaceBuilder() {
        customInit()
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
        customInit()
    }
    
    func customInit() {
        self.clipsToBounds = true
        self.layer.cornerRadius = self.frame.width / 2
    }
}
