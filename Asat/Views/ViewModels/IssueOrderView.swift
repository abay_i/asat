//
//  IssueOrderView.swift
//  Asat
//
//  Created by Александр on 20.08.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit

class IssueOrderView: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var view: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        
    }
    
}
