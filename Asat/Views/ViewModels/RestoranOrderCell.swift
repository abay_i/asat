//
//  RestoranOrderCell.swift
//  Asat
//
//  Created by Александр on 25.09.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit

class RestoranOrderCell: UITableViewCell {

    @IBOutlet weak var clientNameLabel: UILabel!
    
    @IBOutlet weak var adressLabel: UILabel!
    
    @IBOutlet weak var priceLabel: UILabel!
    
    @IBOutlet weak var backView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
