//
//  OrdersTableViewCell.swift
//  Asat
//
//  Created by Александр on 27.08.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit
import Cosmos

class OrdersTableViewCell: UITableViewCell {
    
    var order: Order!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var tsDateLabel: UILabel!
    @IBOutlet weak var tsTimeLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var payButton: UIButton!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var statusImageView: UIImageView!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var backImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    
    @IBAction func payButtonAction(_ sender: UIButton) {
    }
    
    
    
    @IBAction func cancelButtonAction(_ sender: UIButton) {
        if let status = order.status, let vc = sender.parentContainerViewController() {
            if status == 0 {
                
            } else if status == 4 {
                let alert = UIAlertController(title: "Причина", message: "Найти причину", preferredStyle: .alert)
                let ok = UIAlertAction(title: "Закрыть", style: .cancel, handler: nil)
                alert.addAction(ok)
                
                vc.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
