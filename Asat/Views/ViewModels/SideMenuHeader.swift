//
//  SideMenuHeader.swift
//  Asat
//
//  Created by Александр on 14.08.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit

class SideMenuHeader: UITableViewCell {

    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var fioLabel: UILabel!
        
}
