//
//  CounterView.swift
//  Asat
//
//  Created by Александр on 16.08.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit

protocol CounterProtocol {
    func afterChangeValue(value: Int)
}

class CounterView: UIView {
    
    @IBOutlet weak var decrimentButton: UIButton!
    @IBOutlet weak var incrimentButton: UIButton!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var countLabel: UILabel!
    
    var delegate: CounterProtocol?
    
    var value = 0
    var maximunValue = 100
    var minimunValue = 0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commitInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commitInit()
    }
    
    private func commitInit() {
        Bundle.main.loadNibNamed("CounterView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        decrimentButton.addTarget(self, action: #selector(self.decriment), for: .touchUpInside)
        incrimentButton.addTarget(self, action: #selector(self.incriment), for: .touchUpInside)
        
        countLabel.text = value.description
    }
    
    @objc func decriment() {
        guard value != minimunValue else {
            return
        }
        value -= 1
        countLabel.text = value.description
        delegate?.afterChangeValue(value: value)
    }
    
    @objc func incriment() {
        guard value != maximunValue else {
            return
        }
        value += 1
        countLabel.text = value.description
        delegate?.afterChangeValue(value: value)
    }

}
