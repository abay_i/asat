//
//  RestoranCategoriesHeader.swift
//  Asat
//
//  Created by Александр on 29.07.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit
import Cosmos

class RestoranCategoriesHeader: UIView {

    @IBOutlet weak var backgroundImageView: UIImageView!

    @IBOutlet weak var logoImageView: UIImageView!
    
    @IBOutlet weak var minOrderLabel: UILabel!
    
    @IBOutlet weak var deliveryTimeLabel: UILabel!
    
    @IBOutlet weak var deliveryPriceLabel: UILabel!
    
    @IBOutlet weak var workTimeLabel: UILabel!
    
    @IBOutlet weak var ratingView: CosmosView!
    
    @IBOutlet weak var cardYesNoImageView: UIImageView!
    
    @IBOutlet weak var deliveryYesNoImageView: UIImageView!
    
    @IBOutlet weak var halalcardYesNoImageView: UIImageView!
}
