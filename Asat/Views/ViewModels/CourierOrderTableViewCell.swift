//
//  CourierOrderTableViewCell.swift
//  Asat
//
//  Created by Александр on 10.10.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit

class CourierOrderTableViewCell: UITableViewCell {
    
    @IBOutlet weak var fromValueLabel: UILabel!
    @IBOutlet weak var toValueLabel: UILabel!
    @IBOutlet weak var diliverValueLabel: UILabel!
    @IBOutlet weak var backView: UIView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        
        backView.setShadow()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
