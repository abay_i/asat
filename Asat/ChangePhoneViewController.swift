//
//  ChangePhoneViewController.swift
//  Asat
//
//  Created by Abay on 7/6/18.
//  Copyright © 2018 Александр. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class ChangePhoneViewController: ViewController {

    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var phoneTextField: SkyFloatingLabelTextField!
    
    @IBAction func sendAction(_ sender: Any) {
        guard !phoneTextField.text!.isEmpty else {
            alertValidation(message: "Необходимо заполнить «Телефон»", title: "Ошибка")
            return
        }
        
        let hud = self.showProgressHud()
        
        API.changePhone(phone: phoneTextField.text!, compliteHandler: { [weak self] (data, errors, code) -> Void in
            if let error = errors {
                print(error)
                self?.alertValidation(message: error, title: "Не изменить номер телефона")
            } else {
                if let controller = self?.storyboard?.instantiateViewController(withIdentifier: "ConfirmChangePhoneViewController") as? ConfirmChangePhoneViewController {
                    controller.userId = data["user_id"].stringValue
                    self?.navigationController?.pushViewController(controller, animated: true)
                }
            }
            
            self?.hideProgressHud(hud)
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        phoneTextField.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        (appDelegate.window as! WallpaperWindow).wallpaper = #imageLiteral(resourceName: "BackgroundHello")
        
        backgroundImage.image = #imageLiteral(resourceName: "BackgroundHello")
        //let buttonExit = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        let barItem = UIBarButtonItem(title: "Закрыть", style: .plain, target: self, action: #selector(self.exit))
        self.navigationItem.leftBarButtonItem = barItem
    }
    
    
    @objc func exit() {
        self.dismiss(animated: true, completion: {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            (appDelegate.window as! WallpaperWindow).wallpaper = #imageLiteral(resourceName: "BackgroundHello")
        })
    }
}

extension ChangePhoneViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
}
