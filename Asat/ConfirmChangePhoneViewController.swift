//
//  ConfirmChangePhoneViewController.swift
//  Asat
//
//  Created by Abay on 7/6/18.
//  Copyright © 2018 Александр. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class ConfirmChangePhoneViewController: UIViewController {

    var userId: String!
    
    @IBOutlet weak var codeTextField: SkyFloatingLabelTextField!
    
    @IBAction func sendAction(_ sender: Any) {        
        guard !codeTextField.text!.isEmpty else {
            alertValidation(message: "Необходимо заполнить «Код»", title: "Ошибка")
            return
        }

        let hud = self.showProgressHud()

        API.verifyChangePhone(userId: userId, code: codeTextField.text!) { [weak self] data, errors, code in
            guard let strongSelf = self else { return }

            strongSelf.hideProgressHud(hud)

            if let error = errors {
                print(error)
                strongSelf.alertValidation(message: error, title: "Не удалось изменить номер телефона")
            } else {
                strongSelf.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        codeTextField.delegate = self
        
        let navigationBackButton = UIBarButtonItem(title: "Назад", style: .plain, target: self, action: #selector(self.pop))
        self.navigationItem.backBarButtonItem = navigationBackButton
    }
    
    @objc func pop() {
        self.navigationController?.popViewController(animated: true)
    }
}

extension ConfirmChangePhoneViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
}
