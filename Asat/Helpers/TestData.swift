//
//  TestData.swift
//  Emenu
//
//  Created by Александр on 18.06.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import Foundation


class testData {
    
    static func restoransData() -> [[String: Any?]] {
        
        var data: [[String: Any?]] = []
        
        for i in 0..<10 {
            
            let cashback: Int? = i % 2 == 1 ? 5 : nil
            
            let rest = [
                "title": "Some Name \(i)",
                "id": i,
                "cost": 1000 * i,
                "cashback": cashback,
                "time": i * 10
            ] as [String : Any?]
            
            data.append(rest)
            
        }
        
        
        return data
        
    }
    
    static func townsData() -> [String] {
        let towns = [
            "Каркар",
            "Астана",
            "Алмата",
            "Актобе",
            "AAAAAA",
            "Akmolinsk",
            "Караганда"
        ]
        
        return towns
    }
    
}
