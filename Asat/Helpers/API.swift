//
//  Model.swift
//  Emenu
//
//  Created by Александр on 16.06.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import AlamofireObjectMapper
import ObjectMapper

class API {
    
    
    static let baseURL = "http://asat.qlt.kz/api"
    
    static var dics: JSON?
    
    static var header: HTTPHeaders {
        get {
            var headers: HTTPHeaders = [
                 "Content-Type": "application/json",
            ]
            
            if let token = UserDefaults.token {
                print(token)
                headers["Authorization"] = "Bearer \(token)"
            }
            
            return headers
        }
    }
    
    
    /// Регестрирует пользователя в системе
    ///
    /// - Parameters:
    ///   - email: email: String :- email пользователя
    ///   - password: password: String :- password пользователя
    ///   - compliteHandler:
    static func registration(email: String, password: String, phone: String, fio: String, role: String = User.Role.client.rawValue, compliteHandler: @escaping (_ data: JSON, _ errors: String?, _ code: Int) -> ()) {
        
        let params: [String: Any] = [
            "email": email,
            "password": password,
            "phone": phone,
            "fio": fio
        ]
        
        Alamofire.request(self.baseURL + "/users/registration", method: .post, parameters: params, encoding: JSONEncoding.default, headers: self.header).validate().responseJSON { response in
            compliteHandler(JSON(response.data!), response.error?.localizedDescription, response.response!.statusCode)
        }
    }
    
    static func recoverPhone(phone: String, compliteHandler: @escaping (_ data: JSON, _ errors: String?, _ code: Int) -> ()) {
        
        let params: [String: Any] = [
            "phone": phone
        ]
        
        Alamofire.request(self.baseURL + "/users/recover", method: .post, parameters: params, encoding: JSONEncoding.default, headers: self.header).validate().responseJSON { response in
            compliteHandler(JSON(response.data!), response.error?.localizedDescription, response.response!.statusCode)
        }
    }
    
    static func verifyRecoverPhone(userId: String, code: String, newPassword: String, compliteHandler: @escaping (_ data: JSON, _ errors: String?, _ code: Int) -> ()) {
        
        let params: [String: Any] = [
            "id": userId,
            "type": "recovery",
            "code": code,
            "new_password": newPassword
        ]
        
        Alamofire.request(self.baseURL + "/verify", method: .post, parameters: params, encoding: JSONEncoding.default, headers: self.header).validate().responseJSON { response in
            compliteHandler(JSON(response.data!), response.error?.localizedDescription, response.response!.statusCode)
        }
    }
    
    static func changePhone(phone: String, compliteHandler: @escaping (_ data: JSON, _ errors: String?, _ code: Int) -> ()) {
        
        let params: [String: Any] = [
            "phone": phone
        ]
        
        Alamofire.request(self.baseURL + "/users/change-phone", method: .post, parameters: params, encoding: JSONEncoding.default, headers: self.header).validate().responseJSON { response in
            compliteHandler(JSON(response.data!), response.error?.localizedDescription, response.response!.statusCode)
        }
    }
    
    static func verifyChangePhone(userId: String, code: String, compliteHandler: @escaping (_ data: JSON, _ errors: String?, _ code: Int) -> ()) {
        
        let params: [String: Any] = [
            "id": userId,
            "type": "change_phone",
            "code": code
        ]
        
        Alamofire.request(self.baseURL + "/verify", method: .post, parameters: params, encoding: JSONEncoding.default, headers: self.header).validate().responseJSON { response in
            compliteHandler(JSON(response.data!), response.error?.localizedDescription, response.response!.statusCode)
        }
    }
    
    /// Авторизируем пользователя
    ///
    /// - Parameters:
    ///   - login: String
    ///   - password: String
    ///   - compliteHandler: Void
    static func authorization(login: String, password: String, compliteHandler: @escaping (_ data: JSON, _ errors: String?, _ code: Int) -> ()) {
        
        
        let params: [String: Any] = [
            "login": login,
            "password": password
        ]
        
        print(params)
        
        Alamofire.request(self.baseURL + "/users/auth", method: .post, parameters: params, encoding: JSONEncoding.default, headers: self.header).validate(statusCode: 200..<600).responseJSON { response in
            print(response)
            compliteHandler(JSON(response.data!), response.error?.localizedDescription, response.response!.statusCode)
        }
    }
    
    /// Список ресторанов
    ///
    /// - Parameters:
    ///   - params: [String: Any] :- для экспандов
    ///   - compliteHandler: Void
    static func getRestorans(params : [String: Any] = [:], compliteHandler: @escaping (_ data: [Restoran]?, _ errors: String?, _ code: Int?) -> ()) {
        let url = self.baseURL + "/restaurants?city_id=\(UserDefaults.userInfo.city_id!)&expand=locations"

        
        Alamofire.request(self.baseURL + "/restaurants?city_id=\(UserDefaults.userInfo.city_id!)", method: .get, parameters: params, headers: self.header).validate().responseArray { (response: DataResponse<[Restoran]>) in
            print(self.baseURL + "/restaurants?city_id=\(UserDefaults.userInfo.city_id!)")
            if let data = response.result.value {
                compliteHandler(data, nil, response.response!.statusCode)
            } else {
                compliteHandler(nil, response.error?.localizedDescription, response.response?.statusCode)
            }
        }
    }
    
    /// Конкретный ресторанов
    ///
    /// - Parameters:
    ///   - id: Int :- идентификатор ресторана
    ///   - params: [String: Any] :- для экспандов
    ///   - compliteHandler: Void
    static func getRestoran(id: Int, params : [String: Any] = [:], compliteHandler: @escaping (_ data: Restoran?, _ errors: String?, _ code: Int?) -> ()) {
        Alamofire.request(self.baseURL + "/restaurants/\(id)?city_id=\(UserDefaults.userInfo.city_id!)", method: .get, parameters: params, headers: self.header).validate().responseObject { (response: DataResponse<Restoran>) in
            if let data = response.result.value {
                compliteHandler(data, nil, response.response!.statusCode)
            } else {
                compliteHandler(nil, response.error?.localizedDescription, response.response?.statusCode)
            }
        }
    }
    
    /// Получение категорий
    ///
    /// - Parameters:
    ///   - compliteHandler: Void
    static func getMenuCategories(compliteHandler: @escaping (_ data: [MenuCategory]?, _ errors: String?, _ code: Int?) -> ()) {
        Alamofire.request(self.baseURL + "/menu/categories", method: .get, headers: self.header).validate().responseArray { (response: DataResponse<[MenuCategory]>) in
            print(JSON(response.data))
            if let data = response.result.value {
                compliteHandler(data, nil, response.response!.statusCode)
            } else {
                compliteHandler(nil, response.error?.localizedDescription, response.response?.statusCode)
            }
        }
    }
    
    static func getUser(compliteHandler: @escaping (_ data: User?, _ errors: String?, _ code: Int?) -> ()) {
        Alamofire.request(self.baseURL + "/users/profile", method: .get, headers: self.header).validate().responseObject { (response: DataResponse<User>) in
            print(JSON(response.data))
            if let data = response.result.value {
                compliteHandler(data, nil, response.response!.statusCode)
            } else {
                compliteHandler(nil, response.error?.localizedDescription, response.response?.statusCode)
            }
        }
    }
    
    /// Оформление заказа
    ///
    /// - Parameters:
    ///   - params: [String: Any] :- Параметры
    ///   - compliteHandler: Void
    static func sendOrder(params: [String: Any], compliteHandler: @escaping (_ orderId: Int?, _ errors: String?, _ code: Int?) -> ()) {
        Alamofire.request(self.baseURL + "/orders", method: .post, parameters: params, encoding: JSONEncoding.default, headers: self.header).validate().responseObject { (response: DataResponse<Order>) in
            print(params)
            if let data = response.data, let id = JSON(data).dictionary?["order"]?["id"] {
                print(header)
                compliteHandler(id.int, nil, response.response!.statusCode)
            } else {
                print(JSON(response.data))
                compliteHandler(nil, response.error?.localizedDescription, response.response?.statusCode)
            }
        }
    }
    
    /// Получение заказа
    ///
    /// - Parameters:
    ///   - compliteHandler: Void
    static func getOrders(urlString: String = "/orders?sort=-ts&expand=restaurant.info,items.product", compliteHandler: @escaping (_ data: [Order]?, _ errors: String?, _ code: Int?) -> ()) {
//        let url = URL(string: self.baseURL + "/orders")
//        let urlRequest = NSMutableURLRequest(url: url!)
//        urlRequest.allHTTPHeaderFields = self.header
//        urlRequest.cachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        //self.baseURL + "/orders", method: .get, headers: self.header
        
        
        Alamofire.SessionManager.default.requestWithoutCache(self.baseURL + urlString, method: .get, headers: self.header).validate().responseArray { (response: DataResponse<[Order]>) in
            if let data = response.result.value {
                compliteHandler(data, nil, response.response!.statusCode)
            } else {
                compliteHandler(nil, response.error?.localizedDescription, response.response?.statusCode)
            }
        }
    }
    
    static func changeOrder(id: Int, params: [String: Any], compliteHandler: @escaping (_ data: Order?, _ errors: String?, _ code: Int?) -> ()) {
        
        Alamofire.request(self.baseURL + "/orders/\(id)", method: .put, parameters: params, encoding: JSONEncoding.default, headers: self.header).validate().responseObject { (response: DataResponse<Order>) in
            print(JSON(response.data))
            if let data = response.result.value {
                compliteHandler(data, nil, response.response!.statusCode)
            } else {
                compliteHandler(nil, response.error?.localizedDescription, response.response?.statusCode)
            }
        }
    }
    
    /// Получение комментариев ресторана
    ///
    /// - Parameters:
    ///   - restaurant_id: Int
    ///   - compliteHandler: Void
    static func getComments(restaurant_id id: Int, compliteHandler: @escaping (_ data: [Comment]?, _ errors: String?, _ code: Int?) -> ()) {
        Alamofire.request(self.baseURL + "/reviews?restaurant_id=\(id)&expand=user", method: .get, headers: self.header).validate().responseArray { (response: DataResponse<[Comment]>) in
            if let data = response.result.value {
                compliteHandler(data, nil, response.response!.statusCode)
            } else {
                compliteHandler(nil, response.error?.localizedDescription, response.response?.statusCode)
            }
        }
    }
    
    static func getDics(compliteHandler: @escaping (_ data: JSON?, _ errors: String?, _ code: Int?) -> ()) {
        Alamofire.request(self.baseURL + "/dic", method: .get, parameters: nil, headers: nil).response { response in
            if let data = response.data {
                let json = JSON(data)
                self.dics = json
                //let cities = Mapper<City>().mapArray(JSONArray: json[0]["values"].arrayObject as! [[String : Any]])
                compliteHandler(json, nil, response.response!.statusCode)
            } else {
                compliteHandler(nil, response.error?.localizedDescription, response.response?.statusCode)
            }
        }
    }
    
    static func getCouriers(compliteHandler: @escaping (_ data: [User]?, _ errors: String?, _ code: Int?) -> ()) {
        Alamofire.request(self.baseURL + "/couriers", method: .get, headers: self.header).validate().responseArray { (response: DataResponse<[User]>) in
            if let data = response.result.value {
                print(JSON(response.data))
                print(String(data: response.data!, encoding: String.Encoding.utf8))
                compliteHandler(data, nil, response.response!.statusCode)
            } else {
                compliteHandler(nil, response.error?.localizedDescription, response.response?.statusCode)
            }
        }
    }
    
    static func getAddressByLocation(lat: Double, lon: Double, compliteHandler: @escaping (_ data: JSON?, _ errors: String?, _ code: Int?) -> ()) {
        Alamofire.request(self.baseURL + "/geocode/city?lat=\(lat)&lng=\(lon)", method: .get, encoding: JSONEncoding.default, headers: self.header).validate().responseJSON { response in
            compliteHandler(JSON(response.data!), response.error?.localizedDescription, response.response!.statusCode)
        }
    }
    
    static func setPushToken(token: String, compliteHandler: @escaping (_ data: JSON, _ errors: String?, _ code: Int) -> ()) {
        let params: [String: Any] = [
            "token": token,
            "type": 2
        ]
        
        print("YOUR PUSH TOKEN: \(token)")
        
        Alamofire.request(self.baseURL + "/users/push-tokens", method: .post, parameters: params, encoding: JSONEncoding.default, headers: self.header).validate().responseJSON { response in
            compliteHandler(JSON(response.data!), response.error?.localizedDescription, response.response!.statusCode)
        }
    }
    static func changeUser(params: [String: Any], compliteHandler: @escaping (_ data: JSON?, _ errors: String?, _ code: Int?) -> ()) {
        let user = (UIApplication.shared.delegate as! AppDelegate).user
        Alamofire.request(self.baseURL + "/users/base/\(user!.id!)", method: .put, parameters: params, encoding: JSONEncoding.default, headers: self.header).validate().responseJSON { response in
            if let data = response.data{
                print(JSON(data))
                compliteHandler(JSON(data), nil, response.response!.statusCode)
            } else {
                compliteHandler(nil, response.error?.localizedDescription, response.response?.statusCode)
            }
        }
    }
    
    static func getGeocodeId(lat: Double, lon: Double, onComplete:@escaping (Int) -> Void, onError: @escaping (String?) -> Void) {
        let url = self.baseURL + "/geocode/city?lat=\(lat)&lng=\(lon)"
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.header).validate(statusCode: 200..<500).responseJSON { response in
            print(response)
            if response.error == nil {
                if let data = response.data {
                    print(JSON(data))
                    let id = JSON(data)["result"].intValue
                    onComplete(id)
                } else {
                    onError(response.error?.localizedDescription)
                }
            } else {
                onError(response.error?.localizedDescription)
            }
        }
    }
    
    static func getDiscounts(compliteHandler: @escaping (_ data: [Discount]?, _ errors: String?, _ code: Int?) -> ()) {
        Alamofire.request(self.baseURL + "/discounts", method: .get, headers: self.header).validate().responseArray { (response: DataResponse<[Discount]>) in
            if let data = response.result.value {
                compliteHandler(data, nil, response.response!.statusCode)
            } else {
                compliteHandler(nil, response.error?.localizedDescription, response.response?.statusCode)
            }
        }
    }
    
    static func payOrderByEpay(order: Int, compliteHandler: @escaping (_ url: Data?, _ errors: String?, _ code: Int?) -> ()) {
        Alamofire.request(self.baseURL + "/payments/order", method: .post, parameters: ["order_id": order], encoding: JSONEncoding.default, headers: self.header).validate().responseJSON { (response: DataResponse<Any>) in
            if let data = response.data {
                print(header)
                compliteHandler(data, nil, response.response!.statusCode)
            } else {
                compliteHandler(nil, response.error?.localizedDescription, response.response?.statusCode)
            }
        }
    }
    
    static func submitEpay(url: String, params: [String: String], compliteHandler: @escaping (_ data: JSON?, _ errors: String?, _ code: Int?) -> ()) {
        Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: self.header).validate().responseString { (response: DataResponse<String>) in
            print(response)
            print(response.result.value)
            if let data = response.data {
                print(header)
                print(JSON(data))
//                compliteHandler(data, nil, response.response!.statusCode)
            } else {
                compliteHandler(nil, response.error?.localizedDescription, response.response?.statusCode)
            }
        }
    }
    
    static func payOrderByKassa24(order: Int, compliteHandler: @escaping (_ url: Data?, _ errors: String?, _ code: Int?) -> ()) {
        Alamofire.request(self.baseURL + "/onepay/order", method: .post, parameters: ["order_id": order], encoding: JSONEncoding.default, headers: self.header).validate().responseJSON { (response: DataResponse<Any>) in
            if let data = response.data {
                print(header)
                compliteHandler(data, nil, response.response!.statusCode)
            } else {
                compliteHandler(nil, response.error?.localizedDescription, response.response?.statusCode)
            }
        }
    }
    
}


