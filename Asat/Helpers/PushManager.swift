//
//  PushManager.swift
//  BenSnab
//
//  Created by Dmitriy Safarov on 25/08/2017.
//  Copyright © 2017 Dmitriy Safarov. All rights reserved.
//

import Foundation
import Firebase
import FirebaseInstanceID

extension NSNotification.Name {
    
    static let restNewOrderEvent = NSNotification.Name("restNewOrderEvent")
    static let orderPayedEvent = NSNotification.Name("orderPayedEvent")
    static let orderInProcessEvent = NSNotification.Name("orderInProcessEvent")
    static let orderOnTheWayEvent = NSNotification.Name("orderOnTheWayEvent")
    static let orderDeliveredEvent = NSNotification.Name("orderDeliveredEvent")
    static let orderCancelledEvent = NSNotification.Name("orderCancelledEvent")
    static let restCourierAssignedToOrderEvent = NSNotification.Name("restCourierAssignedToOrderEvent")
    static let courierStatusEventChangedEvent = NSNotification.Name("courierStatusEventChangedEvent")
    static let orderShareChangedEvent = NSNotification.Name("orderShareChangedEvent")
    static let courierChangedEvent = NSNotification.Name("courierChangedEvent")

}

class PushManager {

    let gcmMessageIDKey = "gcm.message_id"
    
    static public func getToken() -> String {
        return InstanceID.instanceID().token() ?? ""
    }
    
    static public func updateFCMToken() {
        API.setPushToken(token: PushManager.getToken(), compliteHandler: { (json, error, code) in
            
        })
    }
    
    static let sharedInstance = PushManager()
    
    static public func postNotification(notificationName: NSNotification.Name) {
        NotificationCenter.default.post(name: notificationName, object: nil)
    }
    
    static public func observingNotification(_ observer: Any, notificationName: NSNotification.Name, selector: Selector) {
        NotificationCenter.default.addObserver(observer, selector: selector, name: notificationName, object: nil)
    }
    
}

extension PushManager {
    
    public func processingPushMessage(message: [AnyHashable : Any]) {
        let type = message["type"] as? String ?? ""
        switch type {
        case "1":
            PushManager.postNotification(notificationName: .restNewOrderEvent)
        case "2":
            PushManager.postNotification(notificationName: .orderPayedEvent)
        case "3":
            PushManager.postNotification(notificationName: .orderInProcessEvent)
        case "4":
            PushManager.postNotification(notificationName: .orderOnTheWayEvent)
        case "5":
            PushManager.postNotification(notificationName: .orderDeliveredEvent)
        case "6":
            PushManager.postNotification(notificationName: .orderCancelledEvent)
        case "7":
            PushManager.postNotification(notificationName: .restCourierAssignedToOrderEvent)
        case "8":
            PushManager.postNotification(notificationName: .courierStatusEventChangedEvent)
        case "9":
            PushManager.postNotification(notificationName: .orderShareChangedEvent)
        case "10":
            PushManager.postNotification(notificationName: .courierChangedEvent)
        default:
            print("unknown push type: \(type)")
        }
    }
    
    
}




















