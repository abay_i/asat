//
//  Array.swift
//  Limbo
//
//  Created by Александр on 22.11.16.
//  Copyright © 2016 Александр. All rights reserved.
//

import Foundation


extension Array where Element : Equatable {
    
    mutating func removeObject(_ object : Iterator.Element) {
        if let index = self.index(of: object) {
            self.remove(at: index)
        }
    }
}
