//
//  String.swift
//  Limbo
//
//  Created by Александр on 19.10.16.
//  Copyright © 2016 Александр. All rights reserved.
//

import UIKit
//import SWXMLHash

extension String {
    static func className(_ aClass: AnyClass) -> String {
        return NSStringFromClass(aClass).components(separatedBy: ".").last!
    }
    
    
    func substring(_ from: Int) -> String {
        let start = index(startIndex, offsetBy: from)
        return String(self[start ..< endIndex])
    }
    
    subscript (i: Int) -> Character {
        return self[self.index(self.startIndex, offsetBy: i)]
    }
    
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    
    subscript (r: Range<Int>) -> String {
        let start = index(startIndex, offsetBy: r.lowerBound)
        let end = index(start, offsetBy: r.upperBound - r.lowerBound)
        return String(self[Range(start ..< end)])
    }
    
    var length: Int {
        return self.count
    }
    
    
    var html2AttributedString: NSAttributedString? {
        guard
            let data = data(using: String.Encoding.utf8)
            else { return nil }
        do {
            return try NSAttributedString(data: data, options: [.documentType:NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch let error as NSError {
            print(error.localizedDescription)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
    
    
    /// Returns a string object containing the characters of the
    /// string that lie within a given range.
    ///
    /// Usage:
    ///
    ///  let str = "Hello World!"
    ///  let newString = str.subStringWithRange(6..<12) // "World!"
    
    public func subStringWithRange(_ aRange: Range<Int>) -> String {        
        return String(self[aRange])
    }
    
    /// Replace the given 'subRange' of elements with 'newElements'
    ///
    /// Usage:
    ///
    ///  var str = "Hello World!"
    ///  str.replaceRange(6..<11, with: "Kitty") // "Hello Kitty!"
    
    mutating func replaceRange(_ subRange: Range<Int>, with newElements: String) {
        
        replaceSubrange(converRangeIntToRangeStringIndex(self, range: subRange), with: newElements)
    }
    
    /// Returns a new string in which all occurrences of a target
    /// string in a specified range of the 'String' are replaced by
    /// another given string.
    ///
    /// Usage:
    ///
    ///  var str = "Hello World, World!"
    ///  str = str.stringByReplacingOccurrencesOfString("World", withString: "Kitty", options: NSStringCompareOptions(0), aRange: 6..<11)
    
//    public func stringByReplacingOccurrencesOfString(_ target: String, withString: String, options: NSString.CompareOptions, aRange: CountableRange<Int>!) -> String {
//        
//        let range = aRange == nil ? nil : converRangeIntToRangeStringIndex(self, range: aRange) as Range<String.Index>!
//        
//        
//        return replacingOccurrences(of: target, with: withString, options: options, range: range)
//    }
    
    
    // Localize
    
    func localized(_ lang:String) -> String {
        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
        let bundle = Bundle(path: path!)
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
        
    }
    
    static func getWhiteBlueAttrString(white: String, blue: String, bottomLine: Bool = true) -> NSMutableAttributedString {
        let string = NSMutableAttributedString()
        
        let whiteString = NSAttributedString(string: white, attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        let blueString = NSAttributedString(string: blue, attributes: [
            NSAttributedStringKey.underlineStyle:NSUnderlineStyle.styleSingle.rawValue,
            NSAttributedStringKey.foregroundColor: UIColor(hex: "#039be5")
            ])
        
        
        string.append(whiteString)
        string.append(blueString)
        
        return string
    }
    
}
