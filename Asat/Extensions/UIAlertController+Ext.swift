//
//  UIAlertController+Ext.swift
//  Asat
//
//  Created by Dmitriy Safarov on 14/11/2017.
//  Copyright © 2017 Александр. All rights reserved.
//

import Foundation

extension UIAlertController {
    
    static func showError(viewController : UIViewController, title : String, message : String, handler: (() -> Void)?) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Закрыть", style: .default, handler: { (action) in
            if handler != nil {
                handler!()
            }
        }))
        viewController.present(alert, animated: true, completion: nil)
    }
    
    static func showMessage(viewController : UIViewController, title : String, message : String) {
        showError(viewController: viewController, title: title, message: message, handler: nil)
    }
    
    static func showMessage(viewController : UIViewController, title : String, message : String, handler: (() -> Void)?) {
        showError(viewController: viewController, title: title, message: message, handler: handler)
    }
    
    static func showError(viewController : UIViewController, message : String) {
        showError(viewController: viewController, title: "Закрыть", message: message, handler: nil)
    }
    
}
