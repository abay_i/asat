//
//  UIViewController.swift
//  Limbo
//
//  Created by Александр on 09.10.16.
//  Copyright © 2016 Александр. All rights reserved.
//

import UIKit
import MBProgressHUD

extension UIViewController {
    
    func hideKeyboardWhenTappedAround(_ view: UIView? = nil) {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        
        if view == nil
        {
            self.view.addGestureRecognizer(tap)
        }
        else
        {
            view!.addGestureRecognizer(tap)
        }
        
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func showProgressHud() -> MBProgressHUD
    {
        let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.backgroundView.style = .solidColor
        loadingNotification.bezelView.style = .solidColor
        loadingNotification.bezelView.color = .black
        loadingNotification.bezelView.alpha = 1
        loadingNotification.contentColor = .white
        //loadingNotification.label.text = "Загрузка"
        return loadingNotification
        
    }
    
    func hideProgressHud(_ hud: MBProgressHUD)
    {
        DispatchQueue.main.async {
            hud.hide(animated: true)
        }
    }
    
    func alertValidation(message: String, title: String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "Прожолжить", style: .cancel, handler: nil)
        alert.addAction(ok)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    static func getClassName() -> String {
        return String.className(self)
    }
    
    func setTransculentNavigationBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
    }
    
    func setSideMenuButton() {
        navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: UIImage(named:"menu-options"), style: .plain, target: self, action: #selector(showLeftView(sender:)))
    }
    
    @objc func showLeftView(sender: AnyObject?) {
        sideMenuController?.showLeftView(animated: true, completionHandler: nil)
    }
}
