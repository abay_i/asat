//
//  UserDefaults.swift
//  Emenu
//
//  Created by Александр on 20.06.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import Foundation


extension UserDefaults {
    
    private static var basketIdentifiderKey: String {
        return "userBasketIdentifider"
    }
    
    private static var userUnfoIdentifiderKey: String {
        return "userInfoIdentifider"
    }
    
    private static var appSettingKey: String {
        return "appSettingIdentifider"
    }
    
    static var avatarImageKey: String {
        return "avatarIdentifier"
    }
    
    /// токен пользователя
    static var token: String? {
        get {
            let tokenIdentifiderKey = "userTokenbIdentifider"
            let token = UserDefaults.standard.string(forKey: tokenIdentifiderKey)
            return token
        }
        set (value) {
            let tokenIdentifiderKey = "userTokenbIdentifider"
            UserDefaults.standard.set(value, forKey: tokenIdentifiderKey)
            UserDefaults.standard.synchronize()
        }
    }
    
    // Узнаем запущенно ли приложение в первый раз
    static func isFirstLaunch() -> Bool {
        let hasBeenLaunchedBeforeFlag = "hasBeenLaunchedBeforeFlag"
        let isFirstLaunch = !UserDefaults.standard.bool(forKey: hasBeenLaunchedBeforeFlag)
        if (isFirstLaunch) {
            UserDefaults.standard.set(true, forKey: hasBeenLaunchedBeforeFlag)
            UserDefaults.standard.synchronize()
        }
        return isFirstLaunch
    }
    
    // Получаем корзину
    static var basket: [BasketRestoran] {
        get {
            let data = UserDefaults.standard.object(forKey: self.basketIdentifiderKey) as? Data
            if data != nil {
                let basket = NSKeyedUnarchiver.unarchiveObject(with: data!) as? [BasketRestoran]
                if basket != nil {
                    return basket!
                }
            }
            return []
        }
        set (value) {
            let data = NSKeyedArchiver.archivedData(withRootObject: value)
            UserDefaults.standard.setValue(data, forKey: basketIdentifiderKey)
            UserDefaults.standard.synchronize()
        }
    }
    
    // Информация о адресе
    static var userInfo: UserInfo {
        get {
            if let data = UserDefaults.standard.object(forKey: userUnfoIdentifiderKey) as? Data {
                if let info = NSKeyedUnarchiver.unarchiveObject(with: data) as? UserInfo {
                    return info
                }
            }
            
            return UserInfo()
        }
        set (value) {
            let data = NSKeyedArchiver.archivedData(withRootObject: value)
            UserDefaults.standard.setValue(data, forKey: userUnfoIdentifiderKey)
            UserDefaults.standard.synchronize()
        }
    }
    
    // Параьетры приложения
    
    static var settings: Settings {
        get {
            if let data = UserDefaults.standard.object(forKey: appSettingKey) as? Data {
                if let settings = NSKeyedUnarchiver.unarchiveObject(with: data) as? Settings {
                    return settings
                }
            }
            
            return Settings()
        }
        set (value) {
            let data = NSKeyedArchiver.archivedData(withRootObject: value)
            UserDefaults.standard.setValue(data, forKey: appSettingKey)
            UserDefaults.standard.synchronize()
        }
    }
    
}
