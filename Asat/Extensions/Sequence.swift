//
//  Sequence.swift
//  Asat
//
//  Created by Александр on 13.08.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import Foundation

extension Sequence where Iterator.Element: Hashable {
    func unique() -> [Iterator.Element] {
        var alreadyAdded = Set<Iterator.Element>()
        return self.filter { alreadyAdded.insert($0).inserted }
    }
}
