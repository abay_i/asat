//
//  HIstoryViewController.swift
//  Asat
//
//  Created by Александр on 22.12.2017.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit

class HIstoryViewController: UIViewController {
    
    var orders: [Order] = []
    var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func loadData() {
        let hud = self.showProgressHud()
        API.getOrders(urlString: "/orders?status[]=4&status[]=3&sort=-ts&expand=items.product,restaurant.info", compliteHandler: {(data, error, code) -> Void in
            if let orders = data {
                self.orders = orders
                self.tableView.reloadData()
            } else {
                self.alertValidation(message: error!, title: "Wrong!")
            }
            self.hideProgressHud(hud)

        })
    }
    
    func setTableView() {
        let frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        let table = UITableView(frame: frame, style: .plain)
        self.tableView = table
        self.view.addSubview(tableView)
        table.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "RestoranOrderCell", bundle: nil), forCellReuseIdentifier: "cell")
        tableView.backgroundColor = UIColor.groupTableViewBackground
        tableView.separatorStyle = .none
    }
    

}

extension HIstoryViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! RestoranOrderCell
        
        cell.adressLabel.text = orders[indexPath.row].address
        
        return cell
    }
}

extension HIstoryViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
}
