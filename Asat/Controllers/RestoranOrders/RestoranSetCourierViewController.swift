//
//  RestoranSetCourierViewController.swift
//  Asat
//
//  Created by Александр on 30.09.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit

class RestoranSetCourierViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var order: Order!
    
    var couriers: [User] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        couriers = []
        
        let hud = showProgressHud()
        API.getCouriers(compliteHandler: { (data, error, code) -> Void in
            if let couriers = data {
                self.couriers = couriers
                self.tableView.reloadData()
            } else {
                self.alertValidation(message: "Не удалось загрузить данные!", title: "Ошибка")
            }
            
            self.hideProgressHud(hud)
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}

extension RestoranSetCourierViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return couriers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! RestoranCourierTableViewCell
        
        cell.nameLabel.text = couriers[indexPath.row].fio
        cell.phoneLabel.text = couriers[indexPath.row].phone
        cell.statusLabel.text = couriers[indexPath.row].is_online == 1 ? "online" : "not online"
        cell.statusLabel.textColor = couriers[indexPath.row].is_online == 1 ? .blue : .red
        
        return cell
    }
}

extension RestoranSetCourierViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let timeController = self.storyboard?.instantiateViewController(withIdentifier: "RestoranSetOrderTimeViewController") as! RestoranSetOrderTimeViewController
        timeController.order = self.order
        timeController.driver_id = couriers[indexPath.row].id!
        self.navigationController?.pushViewController(timeController, animated: true)
    }
}
