//
//  RestoranRefuceOrderViewController.swift
//  Asat
//
//  Created by Александр on 01.10.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit

class RestoranRefuceOrderViewController: UIViewController {
    @IBOutlet weak var orderCellView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var refuceButton: UIButton!
    
    var order: Order!
    
    let refuceReasons = ["Нет блюд", "Нет курьеров", "Не работает", "Другое"]
    var selectedReason: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let header = UINib(nibName: "RestoranOrderCell", bundle: nil).instantiate(withOwner: nil, options: nil).first as! RestoranOrderCell
        header.clientNameLabel.text = order.fio
        header.adressLabel.text = order.address
        header.priceLabel.text = order.price?.description
        orderCellView.addSubview(header)
        
        tableView.separatorStyle = .none
        tableView.dataSource = self
        tableView.delegate = self
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        orderCellView.subviews.first!.frame = orderCellView.frame
        orderCellView.subviews.first!.frame.origin = CGPoint(x: 0, y: 0)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func refuceButtonAction(_ sender: UIButton) {
        let hud = self.showProgressHud()
        let params: [String: Any] = [
            "status": 4,
            "cancel_reason": refuceReasons[selectedReason]
        ]
        API.changeOrder(id: order.id!, params: params, compliteHandler: { (data, error, code) -> Void in
            if let _ = data {
                let controller = self.storyboard?.instantiateViewController(withIdentifier: "RestoranOrdersNavigation") as! UINavigationController
                self.sideMenuController?.rootViewController = controller
            } else {
                self.alertValidation(message: "Не удалось отправить заказ", title: "Ошибка")
            }
            
            self.hideProgressHud(hud)
        })
        
    }
   


}


extension RestoranRefuceOrderViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return refuceReasons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        cell.textLabel?.text = refuceReasons[indexPath.row]
        cell.accessoryType = indexPath.row == selectedReason ? .checkmark : .none
        
        return cell
    }
}

extension RestoranRefuceOrderViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedReason = indexPath.row
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
}
