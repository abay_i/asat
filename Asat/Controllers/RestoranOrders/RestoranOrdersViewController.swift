//
//  RestoranOrdersViewController.swift
//  Asat
//
//  Created by Александр on 25.09.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit

class RestoranOrdersViewController: UITableViewController {
    
    
    var orders: [[Order]] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(UINib(nibName: "RestoranOrderCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        self.title = "Обработка заказов"
        self.tableView.backgroundColor = UIColor.groupTableViewBackground
        self.tableView.separatorStyle = .none
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        orders = []
        self.view.isUserInteractionEnabled = false
        let hud = self.showProgressHud()
        API.getOrders(urlString: "/orders?sort=-ts&expand=restaurant.info,items.product&status[]=1&status[]=0", compliteHandler: {(data, error, code) -> Void in
            if let orders = data {
                let noAccessOrders = orders.filter{ $0.status == 0 }
                let accessOrders = orders.filter{ $0.status == 1 }
                self.orders.append(noAccessOrders)
                self.orders.append(accessOrders)
                self.tableView.reloadData()
            } else {
                self.alertValidation(message: error!, title: "Wrong!")
            }
            self.view.isUserInteractionEnabled = true
            self.hideProgressHud(hud)
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return orders.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.orders[section].count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! RestoranOrderCell
        
        cell.clientNameLabel.text = orders[indexPath.section][indexPath.row].fio
        cell.priceLabel.text = orders[indexPath.section][indexPath.row].price!.description + " тг"
        cell.adressLabel.text = orders[indexPath.section][indexPath.row].address
//        cell.selectionStyle = .none
        
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if orders[indexPath.section][indexPath.row].status == 0 {
            let viewOrderController = storyboard?.instantiateViewController(withIdentifier: "RestoranViewOrderViewController") as! RestoranViewOrderViewController
            viewOrderController.order = orders[indexPath.section][indexPath.row]
            self.navigationController?.pushViewController(viewOrderController, animated: true)
        } else {
            let viewOrderController = storyboard?.instantiateViewController(withIdentifier: "RestoranSetCourierViewController") as! RestoranSetCourierViewController
            viewOrderController.order = orders[indexPath.section][indexPath.row]
            self.navigationController?.pushViewController(viewOrderController, animated: true)
        }
        
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 0 ? "Непринятые" : "Принятые"
    }

}
