//
//  RestoranViewOrderViewController.swift
//  Asat
//
//  Created by Александр on 29.09.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit

class RestoranViewOrderViewController: UIViewController {
    
    @IBOutlet weak var cellOrderView: UIView!
    @IBOutlet weak var itemsTableView: UITableView!
    @IBOutlet weak var orderDescStackView: UIStackView!
    
    @IBOutlet weak var fullPriceLabel: UILabel!
    
    @IBOutlet weak var itemTableViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var refuseOrderButton: UIButton!
    @IBOutlet weak var accessOrderButton: UIButton!
    
    var order: Order!
    
    let cellHeight: CGFloat = 30
    let stackViewTextArray = ["Комментарий", "Кол-во персон", "Сдача с купюр", "Промокод", "Кешбек"]
    
    var backView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let header = UINib(nibName: "RestoranOrderCell", bundle: nil).instantiate(withOwner: nil, options: nil).first as! RestoranOrderCell
        header.clientNameLabel.text = order.fio
        header.adressLabel.text = order.address
        header.priceLabel.text = order.price?.description
        
        backView = header.backView!
        
        cellOrderView.addSubview(backView)

        cellOrderView.backgroundColor = .clear


        itemTableViewHeightConstraint.constant = CGFloat(order.getUniqItems().count) * cellHeight
        
        itemsTableView.dataSource = self
        itemsTableView.delegate = self
        
        fullPriceLabel.text = order.price!.description + " тг"
        
        for label in orderDescStackView.arrangedSubviews {
            (label as! UILabel).text = ""
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        backView.frame = cellOrderView.frame
        backView.frame.origin = CGPoint(x: 0, y: 0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        for (index, label) in orderDescStackView.arrangedSubviews.enumerated() {
            (label as! UILabel).text = stackViewTextArray[index]
        }
    }
    
    @IBAction func refuceOrderButtonAction(_ sender: UIButton) {
        let controller = storyboard?.instantiateViewController(withIdentifier: "RestoranRefuceOrderViewController") as! RestoranRefuceOrderViewController
        controller.order = self.order
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
    @IBAction func accessOrderButtonAction(_ sender: UIButton) {
        
//        guard order.status == 0 else {
//            let timeController = self.storyboard?.instantiateViewController(withIdentifier: "RestoranSetCourierViewController") as! RestoranSetCourierViewController
//            timeController.order = self.order
//            self.navigationController?.pushViewController(timeController, animated: true)
//            return
//        }
//        
//        showProgressHud()
//        let params: [String: Any] = [
//            "status": 1
//        ]
//        API.changeOrder(id: order.id!, params: params, compliteHandler: { (data, error, code) -> Void in
//            if let order = data {
//                self.order = order
//                let timeController = self.storyboard?.instantiateViewController(withIdentifier: "RestoranSetCourierViewController") as! RestoranSetCourierViewController
//                timeController.order = self.order
//                self.navigationController?.pushViewController(timeController, animated: true)
//            } else {
//                self.alertValidation(message: "Не удалось отправить заказ", title: "Ошибка")
//            }
//            
//            self.hideProgressHud()
//        })
        
        let timeController = self.storyboard?.instantiateViewController(withIdentifier: "RestoranSetCourierViewController") as! RestoranSetCourierViewController
        timeController.order = self.order
        self.navigationController?.pushViewController(timeController, animated: true)

    }

}

extension RestoranViewOrderViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return order.getUniqItems().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! RestoranViewItemTableViewCell
        
        cell.nameLabel.text = order.getUniqItems()[indexPath.row].product?.name?.description
        cell.countLabel.text = order.getUniqItems()[indexPath.row].count.description
        cell.priceLabel.text = order.getUniqItems()[indexPath.row].price?.description
        
        return cell
    }
}

extension RestoranViewOrderViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeight
    }
}
