//
//  RestoranSetOrderTimeViewController.swift
//  Asat
//
//  Created by Александр on 29.09.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit

class RestoranSetOrderTimeViewController: UIViewController {

    @IBOutlet weak var orderCellView: UIView!
    @IBOutlet weak var timeTextField: UITextField!
    @IBOutlet weak var sendOrderButton: UIButton!
    
    var order: Order!
    var time: String = ""
    var driver_id: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let header = UINib(nibName: "RestoranOrderCell", bundle: nil).instantiate(withOwner: nil, options: nil).first as! RestoranOrderCell
        header.clientNameLabel.text = order.fio
        header.adressLabel.text = order.address
        header.priceLabel.text = order.price?.description
        orderCellView.addSubview(header)
        
        timeTextField.delegate = self
        
        addDoneButtonOnKeyboard()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        orderCellView.subviews.first!.frame = orderCellView.frame
        orderCellView.subviews.first!.frame.origin = CGPoint(x: 0, y: 0)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func sendOrderButtonAction(_ sender: UIButton) {
        let hud = self.showProgressHud()
        let params: [String: Any] = [
            "status": 1,
            "courier_id": driver_id
        ]
        API.changeOrder(id: order.id!, params: params, compliteHandler: { (data, error, code) -> Void in
            if let _ = data {
                let controller = self.storyboard?.instantiateViewController(withIdentifier: "RestoranOrdersNavigation") as! UINavigationController
                self.sideMenuController?.rootViewController = controller
            } else {
                self.alertValidation(message: "Не удалось отправить заказ", title: "Ошибка")
            }
            
            self.hideProgressHud(hud)
        })
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Готова", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.doneButtonAction))
        
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        self.timeTextField.inputAccessoryView = doneToolbar
        
    }
    
    
    
    @objc func doneButtonAction() {
        self.timeTextField.resignFirstResponder()
    }


}

extension RestoranSetOrderTimeViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        time = time + string
        
        if string == "" {
            time = String(time.dropLast())
        }
        
        textField.text = time + " мин."
        
        return false
        
    }
    
    
}
