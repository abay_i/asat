//
//  FirstViewController.swift
//  Asat
//
//  Created by Александр on 16.10.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        (appDelegate.window as! WallpaperWindow).wallpaper = #imageLiteral(resourceName: "BackgroundHello")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let hud = self.showProgressHud()
        
        API.getDics(compliteHandler: { (data, error, code) -> Void in
            if let _ = data {
                self.startApp()
            } else {
                self.alertValidation(message: "Не удалось подключиться к серверу", title: "Ошибка")
            }
            
            self.hideProgressHud(hud)
            
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func startApp() {
        
        if UserDefaults.userInfo != nil && UserDefaults.userInfo.geoValid {
            self.present(MainMenuViewController(), animated: false, completion: nil)
        } else {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            if UserDefaults.token != nil {
                let controller = storyboard.instantiateViewController(withIdentifier: "UserLocationMenuViewController")
                let navigation = UINavigationController(rootViewController: controller)
                controller.setTransculentNavigationBar()
                navigation.navigationBar.backgroundColor = .clear
                self.present(navigation, animated: true, completion: nil)
            } else {
                let controller = storyboard.instantiateViewController(withIdentifier: "AuthNavigationController")
                self.present(controller, animated: false, completion: nil)
            }
            
            
        }
    }
    

}
