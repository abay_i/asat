//
//  AuthViewController.swift
//  Asat
//
//  Created by Александр on 09.07.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit

//rescourier@mail.ru  admin123456
//res2@mail.ru admin12345

class AuthViewController: ViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var authButton: UIButton!
    @IBOutlet weak var registratonButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        let registerTitle = String.getWhiteBlueAttrString(white: "У меня ", blue: "нет аккаунта")
        
        registratonButton.setAttributedTitle(registerTitle, for: .normal)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.backgroundColor = .clear
        self.navigationController?.view.backgroundColor = .clear
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        (appDelegate.window as! WallpaperWindow).wallpaper = #imageLiteral(resourceName: "BackgroundHello")
    }
    

    @IBAction func authButtonAction(_ sender: UIButton) {
        let hud = self.showProgressHud()
        API.authorization(login: emailTextField.text!, password: passwordTextField.text!, compliteHandler: { [weak self] (data, error, code) -> Void in
            if error == nil && code == 201 {
                if let token = data["token"].string {
                    UserDefaults.token = token
                    API.getUser(compliteHandler: { [weak self] (data, error, code) -> Void in
                        self?.hideProgressHud(hud)
                        if let user = data {
                            (UIApplication.shared.delegate as! AppDelegate).user = user
                            PushManager.updateFCMToken()
                            if let info = user.userInfo {
                                UserDefaults.userInfo = info
                                self?.present(MainMenuViewController(), animated: true, completion: nil)
                            } else if user.role == .client {
                                if let controller = self?.storyboard?.instantiateViewController(withIdentifier: "UserLocationMenuViewController") {
                                    self?.navigationController?.pushViewController(controller, animated: true)
                                }
                            } else {
                                self?.present(MainMenuViewController(), animated: true, completion: nil)
                            }

                        } else {
                            (UIApplication.shared.delegate as! AppDelegate).unsetUser()
                            print(error ?? "authorization error")
                            self?.alertValidation(message: "Что то пошло не так", title: "Ошибка")
                        }
                    })
                } else {
                    self?.alertValidation(message: "Что то пошло не так", title: "Ошибка")
                }
            } else {
                self?.alertValidation(message: "Что то пошло не так", title: "Ошибка")
            }
            self?.hideProgressHud(hud)
        })
    }
    
    func getUser() {
        
    }
}
