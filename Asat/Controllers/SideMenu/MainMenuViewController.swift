//
//  MainMenuViewController.swift
//  Asat
//
//  Created by Александр on 11.08.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit
import LGSideMenuController

class MainMenuViewController: LGSideMenuController {
    

    static var user: User?
    
    var isLoaded = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        leftViewPresentationStyle = .scaleFromBig
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        (appDelegate.window as! WallpaperWindow).wallpaper = #imageLiteral(resourceName: "BackgroundHello")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        isLoaded = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard !isLoaded else {
            return
        }
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        
//        guard MainMenuViewController.user == nil else {
//            
//            return
//        }
        
        let left = storyboard.instantiateViewController(withIdentifier: "LeftSideMenu") as! SideMenuViewController
        self.leftViewController = left
        
        if UserDefaults.token != nil {
            
            if let user = (UIApplication.shared.delegate as! AppDelegate).user {
                left.user = user
                self.rootViewController = storyboard.instantiateViewController(withIdentifier: self.getCurrentControllerByUserRole(role: user.role))
                MainMenuViewController.user = user
                left.reload()
                self.rootViewController?.viewWillAppear(true)
                return
            } else {
                let hud = self.showProgressHud()
                API.getUser(compliteHandler: { [weak self] (data, error, code) -> Void in
                    guard let strongSelf = self else { return }
                    strongSelf.hideProgressHud(hud)
                    if let user = data {
                        (UIApplication.shared.delegate as! AppDelegate).user = user
                        left.user = (UIApplication.shared.delegate as! AppDelegate).user
                        strongSelf.rootViewController = storyboard.instantiateViewController(withIdentifier: strongSelf.getCurrentControllerByUserRole(role: user.role))
                        MainMenuViewController.user = user
                        if let info = data?.userInfo {
                            UserDefaults.userInfo = info
                        }
                        left.reload()
                        strongSelf.rootViewController?.viewWillAppear(true)
                    } else {
                        strongSelf.alertValidation(message: "Не удалось получить данные пользователя.\n Вы вошли как Гость", title: "Ошибка")
                    }
                })
            }
        } else {
            self.rootViewController = storyboard.instantiateViewController(withIdentifier: "ResoransList")
            self.rootViewController?.viewWillAppear(true)
        }
        
       
        
        
        
//        self.showProgressHud()
//        
//        API.getUser(compliteHandler: {(data, error, code) -> Void in
//            if error == nil && code == 200 {
//                let left = storyboard.instantiateViewController(withIdentifier: "LeftSideMenu") as! SideMenuViewController
//                (UIApplication.shared.delegate as! AppDelegate).user = data
//                
//                if data?.userInfo != nil && data?.role == User.Role.restaurant && data?.role == User.Role.courier {
//                    UserDefaults.userInfo = data!.userInfo!
//                }
//                if !UserDefaults.userInfo.geoValid {
//                    let userLocationController = storyboard.instantiateViewController(withIdentifier: "UserLocationMenuViewController") as! UserLocationMenuViewController
//                    let navigation = UINavigationController(rootViewController: userLocationController)
//                    userLocationController.setTransculentNavigationBar()
//                    navigation.navigationBar.backgroundColor = .clear
//                    self.present(navigation, animated: true, completion: nil)
//                    return
//                }
//                
//                
//                left.user = data
//                self.leftViewController = left
//                self.rootViewController = storyboard.instantiateViewController(withIdentifier: self.getCurrentControllerByUserRole(role: data!.role))
//                self.rootViewController?.viewWillAppear(true)
//                MainMenuViewController.user = data
//            } else {
//                if !UserDefaults.userInfo.geoValid {
//                    let userLocationController = storyboard.instantiateViewController(withIdentifier: "UserLocationMenuViewController") as! UserLocationMenuViewController
//                    let navigation = UINavigationController(rootViewController: userLocationController)
//                    userLocationController.setTransculentNavigationBar()
//                    navigation.navigationBar.backgroundColor = .clear
//                    self.present(navigation, animated: true, completion: nil)
//                    return
//                }
//                
//                let left = storyboard.instantiateViewController(withIdentifier: "LeftSideMenu") as! SideMenuViewController
//                self.leftViewController = left
//                self.rootViewController = storyboard.instantiateViewController(withIdentifier: "ResoransList")
//                MainMenuViewController.user = data
//                //(UIApplication.shared.delegate as! AppDelegate).unsetUser()
//               // self.alertValidation(message: "Что то пошло не так", title: "Ошибка")
//            }
//            
//            self.hideProgressHud()
//        })
        
        
    }
    
    func getCurrentControllerByUserRole(role: User.Role) -> String {
        switch role {
            case .client:
                return "ResoransList"
            case .restaurant:
                return "RestoranOrdersNavigation"
            case .courier:
                return "CoirierOrdersNav"
            case .none:
                return ""
        }
    }

}
