//
//  SideMenuViewController.swift
//  Emenu
//
//  Created by Александр on 16.06.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit

class SideMenuViewController: UIViewController {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerView: SideMenuHeader!
    @IBOutlet weak var bonusImageView: UIImageView!
    @IBOutlet weak var bonusAmountLabel: UILabel!
    
    var user: User?
    
    var menuData: [String] {
        get {
            if user?.role == .restaurant {
                return ["Обработка заказов", "История", "История", "Насторйки", "Выйти"]
            }
            
            if user?.role == .courier {
                return ["Заказы", "Настройки", "История заказов", "Выйти"]
            }
            
            if user == nil {
                return ["Рестораны", "Акции", "Мой Адрес", "Войти"]
            }
            
            return ["Рестораны", "Акции", "Заказы",  "Чат", "Мой Адрес", "Настройки", "Выйти"]
        }
    }
    var controllersIds: [String] {
        get {
            if user?.role == .restaurant {
                return ["RestoranOrdersNavigation", "HistoryNav", "", ""]
            }
            
            if user?.role == .courier {
                return ["CoirierOrdersNav", "", "HistoryNav"]
            }
            
        
            if user == nil {
                return  ["ResoransList", "ActionsNav", "UserLocationMenuViewController", "Enter"]
            }
            
            return ["ResoransList", "ActionsNav", "OrderNavigation",  "ChatNav", "UserLocationMenuViewController", "SettingsNav", "Exit"]
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerView.photoImageView.layer.cornerRadius = headerView.photoImageView.frame.width / 2
        headerView.photoImageView.clipsToBounds = true
        
        bonusImageView.layer.cornerRadius = bonusImageView.frame.width / 2
        bonusImageView.clipsToBounds = true
        
        if let balance = appDelegate.user?.balance {
            if balance == 0 {
                bonusAmountLabel.text = "0 тг."
            } else {
                bonusAmountLabel.text = "\(balance) тг."
            }
        }
        
//        headerView.photoImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(moveToProfile)))
//        headerView.photoImageView.isUserInteractionEnabled = true
        
//        headerView.fioLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(moveToProfile)))
//        headerView.fioLabel.isUserInteractionEnabled = true
        
        tableView.dataSource = self
        tableView.delegate = self
        
        reload()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadImage()
    }
    
    private func loadImage() {
        DispatchQueue.global(qos: .userInitiated).async {
            if let data = UserDefaults.standard.data(forKey: UserDefaults.avatarImageKey) {
                let image = UIImage(data: data)
                DispatchQueue.main.async { [weak self] in
                    self?.headerView.photoImageView.image = image
                }
            }
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func moveTo(_ controller: UIViewController) {
        sideMenuController?.rootViewController = controller
        sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
    }
    
    @objc func moveToProfile() {
         let controller = storyboard?.instantiateViewController(withIdentifier: "Profile") as! UINavigationController
        self.moveTo(controller)
    }
    
    func reload() {
        headerView.fioLabel.text = user?.fio
        tableView.reloadData()
    }
    
}


extension SideMenuViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        cell.textLabel?.text = menuData[indexPath.row]
        
        switch controllersIds[indexPath.row] {
        case "ResoransList":
            cell.imageView?.image = UIImage(named: "cup")
        case "ActionsNav":
            cell.imageView?.image = UIImage(named: "actions")
        case "OrderNavigation":
            cell.imageView?.image = UIImage(named: "orders")
        case "ChatNav":
            cell.imageView?.image = UIImage(named: "chat")
        case "UserLocationMenuViewController":
            cell.imageView?.image = UIImage(named: "compass")
            tableView.deselectRow(at: indexPath, animated: false)
        case "SettingsNav":
            cell.imageView?.image = UIImage(named: "settings")
        case "Exit":
            cell.imageView?.image = UIImage(named: "exit")
        case "Enter":
            cell.imageView?.image = UIImage(named: "exit")
        default:
            break
        }
        
       
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        cell.selectedBackgroundView = backgroundView
        
        return cell
    }
}


extension SideMenuViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == controllersIds.endIndex - 1 {
            (UIApplication.shared.delegate as! AppDelegate).unsetUser()
            let controller = storyboard?.instantiateViewController(withIdentifier: "AuthNavigationController") as! UINavigationController
            
            if user == nil {
                let helloController = controller.viewControllers.first as! HelloViewController
                helloController.shouldMoveToLogin = true
            } 
            
            present(controller, animated: false, completion: nil)
            return
        }
        
        
        guard !controllersIds[indexPath.row].isEmpty else {
            return
        }
        
        guard sideMenuController?.rootViewController?.restorationIdentifier != controllersIds[indexPath.row] else {
            sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
            return
        }

        let controller = storyboard?.instantiateViewController(withIdentifier: controllersIds[indexPath.row])
        
        if controllersIds[indexPath.row] == "UserLocationMenuViewController" {
            let navigation = UINavigationController(rootViewController: controller!)
            controller?.setTransculentNavigationBar()
            (controller as! UserLocationMenuViewController).setWindow = false
            self.present(navigation, animated: true, completion: nil)
            sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
            return 
        }
        
        moveTo(controller!)
    }
}
