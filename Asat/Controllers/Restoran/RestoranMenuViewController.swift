//
//  RestoranMenuViewController.swift
//  Emenu
//
//  Created by Александр on 18.06.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class RestoranMenuViewController: UIViewController, IndicatorInfoProvider {
    
    let tableViewHeaderHeight: CGFloat = 250
    
    var itemInfo: IndicatorInfo = ""
    
    var parentController: RestoranTabsProtocol?
    
    var categories: [MenuCategory] = []
    var categoriesDic: [MenuCategory] = []
    var menu: [RestoransMenu] = []
    var restoran: Restoran!
    
    @IBOutlet weak var tableView: UITableView!
    
    init(itemInfo: IndicatorInfo) {
        self.itemInfo = itemInfo
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        restoran = parentController?.getMainController!()?.restoran
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.tableFooterView = UIView()
        
        
        let restoranHeader = Bundle.main.loadNibNamed("RestoranCategoriesHeader", owner: self, options: nil)?.first as! RestoranCategoriesHeader
        
        title = restoran.name
        
        let info = restoran.info
        
        if let photo = info?.photo {
            restoranHeader.backgroundImageView.kf.setImage(with: URL(string: photo), options: [.transition(.fade(0.5))])
        }
        
        if let logo = info?.logo {
            restoranHeader.logoImageView.kf.setImage(with: URL(string: logo), options: [.transition(.fade(0.5))])
        }
        
        if let minPrice = restoran.min_price_description {
            restoranHeader.minOrderLabel.text = minPrice
        }
        
        if let rating = restoran.rating {
            restoranHeader.ratingView.rating = Double(rating)
        }
        
        if let workStart = restoran.work_start, let workEnd = restoran.work_end {
            restoranHeader.workTimeLabel.text = "\(workStart)-\(workEnd)"
        }
        
        if let onlinePayment = restoran.online_payment, onlinePayment {
            restoranHeader.cardYesNoImageView.image = UIImage(named: "yes")
        } else {
            restoranHeader.cardYesNoImageView.image = UIImage(named: "no")
        }
        
        if let pickup = restoran.pickup, pickup {
            restoranHeader.deliveryYesNoImageView.image = UIImage(named: "yes")
        } else {
            restoranHeader.deliveryYesNoImageView.image = UIImage(named: "no")
        }
        
        if let locations = restoran.locations {
            if locations.count > 0 {
                if let time = locations[0].delivery_time {
                    restoranHeader.deliveryTimeLabel.text = "\(time) мин"
                }
                if let price = locations[0].price {
                    restoranHeader.deliveryPriceLabel.text = "\(price) тг"
                }
            }
        }
        
        tableView.tableHeaderView = restoranHeader
    }

    override func viewDidLayoutSubviews() {
        // viewDidLayoutSubviews is called when labels change.
        super.viewDidLayoutSubviews()
        sizeHeaderToFit(tableView)
    }
    
    func sizeHeaderToFit(_ tableView: UITableView) {
        if let headerView = tableView.tableHeaderView {
            let height = tableViewHeaderHeight
            var frame = headerView.frame
            frame.size.height = height
            headerView.frame = frame
            tableView.tableHeaderView = headerView
            headerView.setNeedsLayout()
            headerView.layoutIfNeeded()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }

    func reload() {
        var cats_ids: [Int] = []
        for item in menu {
            cats_ids.append(item.category_id!)
        }
        cats_ids = cats_ids.unique()
        categories = categoriesDic.filter({(category) -> Bool in
            return cats_ids.contains(category.id!)
        })
        
        tableView.reloadData()
    }

}


extension RestoranMenuViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        cell.textLabel?.text = categories[indexPath.row].name
        cell.textLabel?.textColor = .gray
        
        return cell
    }
}


extension RestoranMenuViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "MenuParentViewController") as! MenuParentViewController
        
        controller.categories = categories
        controller.menu = menu
        controller.index = indexPath.row        
        controller.restoran = restoran
        
        self.parentController?.move?(to: controller)
    }
}
