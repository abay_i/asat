//
//  RestoranTabsController.swift
//  Emenu
//
//  Created by Александр on 18.06.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit
import XLPagerTabStrip

@objc protocol RestoranTabsProtocol {
    @objc optional func move(to controller: UIViewController)
    @objc optional func getMainController() -> RestoranViewController?
}

class RestoranTabsController: ButtonBarPagerTabStripViewController {

    let tabData: [IndicatorInfo] = ["Меню", "Отзывы", "Инфо"]
    
    //var categoriesDic: [MenuCategory] = []
    var restoran: Restoran?
    var restoran_id: Int!
    
    var mainViewController: RestoranViewController?
    
    var menuController: RestoranMenuViewController!
    var commentController: RestoranCommentsViewController!
    var infoController: RestoranInfoViewController!
    
    override func viewDidLoad() {
        
        settings.style.buttonBarItemLeftRightMargin = 0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemBackgroundColor = UIColor.white
        settings.style.buttonBarItemTitleColor = .lightGray
        settings.style.buttonBarHeight = 60
        
        super.viewDidLoad()

        buttonBarView.backgroundColor = .white
        buttonBarView.selectedBar.backgroundColor = UIColor(hex: "#039be5")
        // Do any additional setup after loading the view.
        loadRestoran()
    }
    
    private func loadRestoran() {
        let hud = self.mainViewController?.showProgressHud()
        API.getRestoran(id: self.restoran_id, params: ["expand": "info,menu,reviews.user"], compliteHandler: { [weak self] (data, error, code) -> Void in
            guard let strongSelf = self else { return }
            if let restoran = data {
                strongSelf.restoran = restoran
                if let menu = restoran.menu {
                    strongSelf.menuController.menu = menu
                }
                if let comments = restoran.reviews {
                    strongSelf.commentController.comments = comments
                }
                if let info = restoran.info {
                    strongSelf.infoController.info = info
                }
                //                strongSelf.mainViewController?.title = strongSelf.restoran!.name
                //                strongSelf.mainViewController?.restoranHeader.backgroundImageView.kf.setImage(with: URL(string: restoran.info!.photo!), options: [.transition(.fade(1.2))])
                //                strongSelf.mainViewController?.restoranHeader.logoImageView.kf.setImage(with: URL(string: restoran.logo!), options: [.transition(.fade(1.2))])
                //                strongSelf.mainViewController?.restoranHeader.minOrderLabel.attributedText = String.getWhiteBlueAttrString(white: "Мин.заказ: ", blue: restoran.min_price!.description, bottomLine: false)
                //                strongSelf.mainViewController?.restoranHeader.restoranNameLAbel.text = restoran.name
            } else {
                print(error!, code!) // Сюда алерт с ошибкой
            }
            
            API.getMenuCategories(compliteHandler: { [weak self] (data, error, code) -> Void in
                guard let strongSelf = self else { return }
                if let cats = data {
                    strongSelf.menuController.categoriesDic = cats
                    strongSelf.menuController.reload()
                } else {
                    print(error!, code!) // Сюда алерт с ошибкой
                }
                
                if let hud = hud {
                    strongSelf.mainViewController?.hideProgressHud(hud)
                }
            })
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        menuController = RestoranMenuViewController(itemInfo: tabData[0]) //RestoranMenuCategoryViewController(itemInfo: tabData[0])
        commentController = RestoranCommentsViewController(itemInfo: tabData[1])
        infoController = RestoranInfoViewController(itemInfo: tabData[2])
        
        
        menuController.parentController = self
        commentController.parentController = self
        infoController.parentController = self
        
        return [menuController, commentController, infoController]
    }
    
    override func configureCell(_ cell: ButtonBarViewCell, indicatorInfo: IndicatorInfo) {
        super.configureCell(cell, indicatorInfo: indicatorInfo)
       // cell.backgroundColor = .blue
    }

}


extension RestoranTabsController: RestoranTabsProtocol {
    func move(to controller: UIViewController) {
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func getMainController() -> RestoranViewController? {
        return self.mainViewController
    }
}
