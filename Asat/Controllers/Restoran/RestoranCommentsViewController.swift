//
//  RestoranCommentsViewController.swift
//  Emenu
//
//  Created by Александр on 18.06.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class RestoranCommentsViewController: UIViewController, IndicatorInfoProvider {

    var itemInfo: IndicatorInfo = ""
    var parentController: RestoranTabsProtocol?
    
    var comments: [Comment] = []
    let minimunCellHeight: CGFloat = 80
    
    @IBOutlet weak var tableView: UITableView!
    
    init(itemInfo: IndicatorInfo) {
        self.itemInfo = itemInfo
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.groupTableViewBackground
        
        tableView.register(UINib(nibName: "RestoranReviewCell", bundle: nil), forCellReuseIdentifier: "cell")
        tableView.dataSource = self
        tableView.delegate = self
        tableView.estimatedRowHeight = 80
        tableView.rowHeight = UITableViewAutomaticDimension
        
        tableView.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        print(comments.isEmpty, comments)
//        
//        guard comments.isEmpty else {
//            return
//        }
//        
//        self.parentController?.getMainController!()?.view.showProgressHud()
//        
//        API.getComments(restaurant_id: self.parentController!.getMainController!()!.restoranId, compliteHandler: {(data, error, code) in
//            if let comments = data {
//                self.comments = comments
//                print(comments)
//                self.tableView.reloadData()
//            } else {
//                print(error!)
//            }
//            
//            self.parentController?.getMainController!()?.view.hideProgressHud()
//        })
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }

}

extension RestoranCommentsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! RestoranReviewCell
        
        cell.commentLabel.text = comments[indexPath.row].comment
        cell.fioLabel.text = comments[indexPath.row].user?.fio
        
        if let photo = comments[indexPath.row].user?.photo {
            print()
            cell.avatarImageView.kf.setImage(with: URL(string: photo), options: [.transition(.fade(1.2))])
        }
        
        
        return cell
    }
}

extension RestoranCommentsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

}

