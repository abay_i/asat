//
//  RestoranViewController.swift
//  Asat
//
//  Created by Александр on 12.08.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit
import MXParallaxHeader
import Kingfisher

class RestoranViewController: ClientViewController {
    
    var restoranHeader: RestoranCategoriesHeader!
    var contentView: UIView!
    
    var restoran: Restoran!
    var restoranId: Int!
    
    var restoranTabsController: RestoranTabsController!

    @IBOutlet weak var scrollView: MXScrollView!

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = restoran.name
       
        scrollView.parallaxHeader.minimumHeight = (self.navigationController?.navigationBar.intrinsicContentSize.height)! + UIApplication.shared.statusBarFrame.height
        
        restoranTabsController = storyboard?.instantiateViewController(withIdentifier: "restoranTabsController") as! RestoranTabsController
        restoranTabsController.mainViewController = self
        restoranTabsController.restoran_id = restoranId
        self.contentView = self.restoranTabsController.view
        self.scrollView.addSubview(self.contentView)
        self.addChildViewController(self.restoranTabsController)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        var frame = view.frame
        
        scrollView.frame = frame
        scrollView.contentSize = frame.size

        frame.size.height -= scrollView.parallaxHeader.minimumHeight
        contentView.frame = frame
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}


