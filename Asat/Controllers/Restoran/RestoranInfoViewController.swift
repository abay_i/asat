//
//  RestoranInfoViewController.swift
//  Emenu
//
//  Created by Александр on 18.06.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class RestoranInfoViewController: UIViewController, IndicatorInfoProvider {

    var itemInfo: IndicatorInfo = ""
    var parentController: RestoranTabsProtocol?
    
    var info: ResoranInfo?
    
    init(itemInfo: IndicatorInfo) {
        self.itemInfo = itemInfo
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let label = UILabel()
        label.frame = CGRect(x: 8, y: 8, width: self.view.frame.width - 16, height: 100)
        label.font = UIFont.systemFont(ofSize: 14)
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        
        var string = ""
        
        if let info = self.info {
            string = "Адресс: \(info.address!)\n\nКонтакты: \(info.contacts!)\n\nОписание: \(info.description!)"
        }
        
        label.text = string
        self.view.addSubview(label)
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }

}
