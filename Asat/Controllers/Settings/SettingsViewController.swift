//
//  SettingsViewController.swift
//  Asat
//
//  Created by Александр on 04.10.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class SettingsViewController: UIViewController {
        
    @IBOutlet weak var avatarImageView: UIImageView!
    
    @IBOutlet weak var fioSkyTextFiled: SkyFloatingLabelTextField!
    @IBOutlet weak var phoneSkyTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var emailSkyTextField: SkyFloatingLabelTextField!
    
    @IBOutlet weak var doneButton: UIButton!
    
    var imagePicker = UIImagePickerController()
    
    var paymentData: [String] = []
    var settings = UserDefaults.settings
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        paymentData = ["Картой", "Наличные", "Бонусы"]
        
        imagePicker.delegate = self
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(setImage))
        tap.numberOfTapsRequired = 1
        
        avatarImageView.layer.cornerRadius = avatarImageView.frame.width / 2
        avatarImageView.addGestureRecognizer(tap)
        avatarImageView.isUserInteractionEnabled = true
        avatarImageView.clipsToBounds = true
        
        setSideMenuButton()
        
        
        loadImage()
    }
    
    private func loadImage() {
        DispatchQueue.global().async {
            if let data = UserDefaults.standard.data(forKey: UserDefaults.avatarImageKey) {
                let image = UIImage(data: data)
                DispatchQueue.main.async { [weak self] in
                    self?.avatarImageView.image = image
                }
            }
        }
    }
    
    @IBAction func changePhoneAction(_ sender: Any) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ChangePhoneViewController") as! ChangePhoneViewController
            let navigation = UINavigationController(rootViewController: controller)
            controller.setTransculentNavigationBar()
            self.present(navigation, animated: true, completion: nil)
            sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
            return
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let app = (UIApplication.shared.delegate as! AppDelegate)
        
        if let user = app.user {
            fioSkyTextFiled.text = user.fio
            phoneSkyTextField.text = user.phone
            emailSkyTextField.text = user.email
//            if let photo = user.photo {
//                avatarImageView.kf.setImage(with: URL(string: photo), options: [.transition(.fade(1.2))])
//            }
        }
    }

    @IBAction func doneButtonAction(_ sender: UIButton) {
        let hud = showProgressHud()
        let params: [String: Any] = [
            "fio": fioSkyTextFiled.text!,
            ]
        API.changeUser(params: params, compliteHandler: { [weak self] (data, error, code) -> Void in
            guard let strongSelf = self else { return }
            if let _ = data {
                (UIApplication.shared.delegate as! AppDelegate).user?.fio = strongSelf.fioSkyTextFiled.text!
                //                (UIApplication.shared.delegate as! AppDelegate).user?.phone = strongSelf.phoneSkyTextField.text!
                //                (UIApplication.shared.delegate as! AppDelegate).user?.email = strongSelf.emailSkyTextField.text!
//                strongSelf.alertValidation(message: "Настройки сохранены", title: "Готова!")
                (strongSelf.sideMenuController?.leftViewController as! SideMenuViewController).reload()
            } else {
                strongSelf.alertValidation(message: "Не удалось сохранить данные", title: "Ошибка")
            }
            strongSelf.hideProgressHud(hud)
        })
    }
    
    @objc func setImage() {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")
            
            
            imagePicker.sourceType = .savedPhotosAlbum;
            imagePicker.allowsEditing = false
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
}




extension SettingsViewController: UIImagePickerControllerDelegate {
    
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!){
        dismiss(animated: true, completion: { [weak self] () -> Void in
            self?.avatarImageView.image = image
            let data = UIImagePNGRepresentation(image)
            UserDefaults.standard.set(data, forKey: UserDefaults.avatarImageKey)
        })
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            avatarImageView.contentMode = .scaleAspectFill
            avatarImageView.image = pickedImage
            let data = UIImagePNGRepresentation(pickedImage)
            UserDefaults.standard.set(data, forKey: UserDefaults.avatarImageKey)
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
}

extension SettingsViewController: UINavigationControllerDelegate {}
