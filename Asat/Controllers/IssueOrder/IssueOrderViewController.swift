//
//  IssueOrderViewController.swift
//  Asat
//
//  Created by Александр on 24.08.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import SwiftyJSON

class IssueOrderViewController: UIViewController {
    
    private enum PaymentType {
        case card
        case balance
    }
    
    @IBOutlet weak var fioSkyTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var phoneSkyTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var addressSkyTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var commnetSkyTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var personsNumberSkyTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var shortChangeSkyTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var issueOrderView: IssueOrderView!
    @IBOutlet weak var issueTypeSegment: UISegmentedControl!
    @IBOutlet weak var preOrderTimeButton: UIButton!
    @IBOutlet weak var cardPaymentCheckImage: UIImageView!
    @IBOutlet weak var balancePaymentCheckImage: UIImageView!
    @IBOutlet weak var preOrderSwitch: UISwitch!
    
    private let emptyTimeTemplate = ":"
    private var paymentType: PaymentType = .card
    
    var restoran_id: Int!
    
    @IBAction func onPreOrderSwitchAction(_ sender: UISwitch) {
        if sender.isOn {
            showDatePicker()
        } else {
            preOrderTimeButton.setTitle(emptyTimeTemplate, for: .normal)
        }
    }
    
    private func showDatePicker() {
        let calendar = Calendar.current
        let alert = UIAlertController(style: .actionSheet, title: "Время предзаказа")
        var selectedDate = Date()
        alert.addDatePicker(mode: .time, date: nil) { date in
            selectedDate = date
        }
        alert.addAction(title: "Выбрать", style: .cancel) { [weak self] _ in
            let hour = calendar.component(.hour, from: selectedDate)
            let minutes = calendar.component(.minute, from: selectedDate)
            print(selectedDate)
            self?.preOrderTimeButton.setTitle("\(hour):\(minutes)", for: .normal)
        }
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func preOrderAction(_ sender: UIButton) {
        if preOrderSwitch.isOn {
            showDatePicker()
        }
    }
    
    @IBAction func onCardPaymentTypeAction(_ sender: UIButton) {
        if paymentType == .balance {
            paymentType = .card
            cardPaymentCheckImage.image = UIImage(named: "Ellipse 10")
            balancePaymentCheckImage.image = nil
        }
    }
    
    @IBAction func onBalancePaymentTypeAction(_ sender: UIButton) {
        if paymentType == .card {
            paymentType = .balance
            balancePaymentCheckImage.image = UIImage(named: "Ellipse 10")
            cardPaymentCheckImage.image = nil
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        
        let price = BasketList.data[BasketList.findRestoranById(restoran_id)!.section].allPrice
        
        self.issueOrderView.priceLabel.text = "\(price) тг."
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.sendOrder))
        tap.numberOfTapsRequired = 1
        issueOrderView.addGestureRecognizer(tap)
        issueOrderView.isUserInteractionEnabled = true
        
        cardPaymentCheckImage.image = UIImage(named: "Ellipse 10")
        preOrderTimeButton.setTitle(emptyTimeTemplate, for: .normal)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let app = (UIApplication.shared.delegate as! AppDelegate)
        
        if let user = app.user {
            fioSkyTextField.text = user.fio
            phoneSkyTextField.text = user.phone
        }
        
        addressSkyTextField.text = UserDefaults.userInfo.address
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func sendOrder() {
        
        print(restoran_id)
        let restoransCarts = BasketList.generateCart(restoran_id)
        let index = BasketList.findRestoranById(restoran_id)!.section
        
        guard !restoransCarts.isEmpty else {
            return
        }
        
        guard BasketList.data[index].allPrice >= BasketList.data[index].restoran.min_price! else {
            alertValidation(message: "Цена заказа должна быть больше или равна \(BasketList.data[index].restoran.min_price!)", title: nil)
            return
        }
        
        var params: [String: Any] = [
            "restaurant_id": restoransCarts.first!["restoran_id"]!,
            "cart": restoransCarts.first!["cart"]!,
            "type": issueTypeSegment.selectedSegmentIndex + 1,
            "address": self.addressSkyTextField.text!,
            "city_id": UserDefaults.userInfo.city_id!,
            "comment": self.commnetSkyTextField.text!,
            "persons": self.personsNumberSkyTextField.text!,
            "payment_type": 1,
            "status": 0
        ]
        
        if let preordertime = preOrderTimeButton.titleLabel?.text, preOrderTimeButton.titleLabel?.text != emptyTimeTemplate, preOrderSwitch.isOn {
            params["preorder"] = "\(preordertime)"
        }
        
        print(params)
        
        let hud = self.showProgressHud()
        API.sendOrder(params: params, compliteHandler: {[weak self] (orderId, error, code) -> Void in
            guard let strongSelf = self else { return }
            if let id = orderId {
                //print(UserDefaults.token)
                BasketList.remove(by: strongSelf.restoran_id, important: true)
                strongSelf.hideProgressHud(hud)                
                strongSelf.showPaymentAlert(order: id)
                
            } else {
                print(error)
                strongSelf.hideProgressHud(hud)
            }
        })
    }
    
    private func showPaymentAlert(order id: Int) {
        if paymentType == .card {
            let alert = UIAlertController(title: "", message: "Выберите вариант оплаты", preferredStyle: .actionSheet)
            let later = UIAlertAction(title: "Оплатить позже", style: .default) { [weak self] _ in
                self?.moveToOrders()
            }
            let epay = UIAlertAction(title: "Epay", style: .default) { [weak self] _ in
                guard let strongSelf = self else { return }
                let hud = strongSelf.showProgressHud()
                API.payOrderByEpay(order: id) { [weak self] data, error, code in
                    guard let strongSelf = self else { return }
                    strongSelf.hideProgressHud(hud)
                    print(JSON(data))
                    if let data = data, let dict = JSON(data).dictionary, let url = dict["action"]?.string {
                        print(url)
                        
                        let app = (UIApplication.shared.delegate as! AppDelegate)
                        
                        if let user = app.user, let email = user.email {
                            let params: [String: String] = [
                                "email": email,
                                "BackLink": dict["BackLink"]!.string!,
                                "PostLink": dict["PostLink"]!.string!,
                                "Signed_Order_B64": dict["Signed_Order_B64"]!.string!,
                                "FailurePostLink": dict["FailurePostLink"]!.string!,
                                "appendix": "PGRvY3VtZW50PjxpdGVtIG51bWJlcj0iMSIgbmFtZT0i0KLQtdC70LXRhNC+0L3QvdGL0Lkg0LDQv9C/0LDRgNCw0YIiIHF1YW50aXR5PSIyIiBhbW91bnQ9IjEwMDAiLz48aXRlbSBudW1iZXI9IjIiIG5hbWU9ItCo0L3Rg9GAIDLQvC4iIHF1YW50aXR5PSIyIiBhbW91bnQ9IjIwMCIvPjwvZG9jdW1lbnQ+"
                                ]
                            
                            API.submitEpay(url: url, params: params) { [weak self] data, error, code in
                                
                            }
                        }
                        
                        
//                        self?.open(url)
//                        self?.moveToOrders()
                    } else {
                        print(error)
                    }
                }
            }
            
            let kassa = UIAlertAction(title: "Касса 24", style: .default) { [weak self] _ in
                guard let strongSelf = self else { return }
                let hud = strongSelf.showProgressHud()
                API.payOrderByKassa24(order: id) { [weak self] data, error, code in
                    guard let strongSelf = self else { return }
                    strongSelf.hideProgressHud(hud)
                    if let data = data, let sUrl = JSON(data).dictionary?["formUrl"]?.string, let url = URL(string: sUrl) {
                        print(sUrl)
                        self?.open(url)
                        self?.moveToOrders()                        
                    } else {
                        print(error)
                    }
                }
            }
            
            alert.addAction(epay)
            alert.addAction(kassa)
            alert.addAction(later)
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    private func open(_ url: URL) {
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    private func moveToOrders() {
        let controller = storyboard?.instantiateViewController(withIdentifier: "OrderNavigation") as! UINavigationController
        sideMenuController?.rootViewController = controller
    }
    
    @IBAction func issueTypeSegmentAction(_ sender: UISegmentedControl) {
        print(sender.selectedSegmentIndex)
    }
    
}
