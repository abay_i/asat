//
//  RestoranMenuCategoryViewController.swift
//  Asat
//
//  Created by Александр on 29.07.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class RestoranMenuCategoryViewController: UIViewController, IndicatorInfoProvider {

    @IBOutlet weak var tableView: UITableView!
    
    var itemInfo: IndicatorInfo = ""
    var parentController: RestoranTabsProtocol?
    
    var tableViewHeader: RestoranCategoriesHeader!
    
    private let tableViewHeaderHeightMax: CGFloat = 200.0
    
    init(itemInfo: IndicatorInfo) {
        self.itemInfo = itemInfo
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableViewHeader = Bundle.main.loadNibNamed("RestoranCategoriesHeader", owner: self, options: nil)?.first as! RestoranCategoriesHeader
        

        tableView.tableHeaderView = nil
        tableView.addSubview(tableViewHeader)
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.contentInset = UIEdgeInsets(top: tableViewHeaderHeightMax, left: 0, bottom: 0, right: 0)
        tableView.contentOffset = CGPoint(x: 0, y: -tableViewHeaderHeightMax)
        
        
        updateHeaderView()
    }

    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
    
    func updateHeaderView(){
        var headerRect = CGRect(x: 0, y: -tableViewHeaderHeightMax, width: tableView.bounds.width, height: tableViewHeaderHeightMax)
        
        if tableView.contentOffset.y < -tableViewHeaderHeightMax
        {
            headerRect.origin.y = tableView.contentOffset.y
            headerRect.size.height = -tableView.contentOffset.y
        }
        
        tableViewHeader.frame = headerRect
    }
    
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        updateHeaderView()
//    }


}


extension RestoranMenuCategoryViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        cell.textLabel?.text = "categoty 1"
        
        return cell
    }
}

extension RestoranMenuCategoryViewController: UITableViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.updateHeaderView()
    }
}
