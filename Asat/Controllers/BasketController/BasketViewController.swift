//
//  BasketViewController.swift
//  Asat
//
//  Created by Александр on 17.08.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit

class BasketViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        BasketList.initBasket()
        
        
        
        title = "Корзина"

        tableView.register(UINib(nibName: "RestoranMenuCell", bundle: nil), forCellReuseIdentifier: "cell")
       // tableView.register(UINib(nibName: "IssueOrderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "footer")
        tableView.backgroundColor = UIColor.groupTableViewBackground
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        
        reload()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func reload() {
        tableView.reloadData()
    }
    
    @objc func moveToIssue(sender: UITapGestureRecognizer) {
        let app = (UIApplication.shared.delegate as! AppDelegate)
        
        if app.user != nil {
            let controller = storyboard?.instantiateViewController(withIdentifier: "IssueOrderController") as! IssueOrderViewController
            controller.restoran_id = sender.view!.tag
            self.navigationController?.pushViewController(controller, animated: true)
        } else {
            let controller = storyboard?.instantiateViewController(withIdentifier: "AuthNavigationController") as! UINavigationController
            let helloController = controller.viewControllers.first as! HelloViewController
            helloController.shouldMoveToLogin = true
            present(controller, animated: false, completion: nil)
        }
    }
    
    
    @IBAction func clearAction(_ sender: UIBarButtonItem) {
        UserDefaults.basket = []
        BasketList.initBasket()
        reload()
    }

}


extension BasketViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return BasketList.data.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return BasketList.data[section].restoran.name
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return BasketList.data[section].foods.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! RestoranMenuTableViewCell
        
        cell.nameLabel.text = BasketList.data[indexPath.section].foods[indexPath.row].food.name
        cell.priceLabel.text = BasketList.data[indexPath.section].foods[indexPath.row].food.price!.description + " тг"
        if let photo = BasketList.data[indexPath.section].foods[indexPath.row].food.photo {
            cell.foodImageView.kf.setImage(with: URL(string: photo), options: [.transition(.fade(1.2))])
        }
        cell.indexPath = indexPath
        cell.delegate = self
        
        cell.counterView.value = 0
        
        if let foodInBascet = BasketList.findIndex(by: BasketList.data[indexPath.section].foods[indexPath.row].food.id!) {
            cell.priceLabel.text = cell.priceLabel.text!  //+ " * \(foodInBascet.basket.count)"
            cell.counterView.value = foodInBascet.basket.count
            cell.counterView.countLabel.text = foodInBascet.basket.count.description
        }
        
        return cell
    }
}

extension BasketViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let issueView = tableView.dequeueReusableCell(withIdentifier: "footer") as! IssueOrderView
        issueView.priceLabel.text = BasketList.data[section].allPrice.description + " тг"
        
        issueView.tag = BasketList.data[section].restoran.id!
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.moveToIssue(sender:)))
        tap.numberOfTapsRequired = 1
        
        issueView.addGestureRecognizer(tap)
        issueView.isUserInteractionEnabled = true

        return issueView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 60
    }
}

extension BasketViewController: RestoranMenuTableViewCellProtocol {
    func counterWasChanged(indexPath: IndexPath, count: Int) {
        BasketList.changeOrAdd(restoran: BasketList.data[indexPath.section].restoran, food: BasketList.data[indexPath.section].foods[indexPath.row].food, count: count)
        reload()
    }
}
