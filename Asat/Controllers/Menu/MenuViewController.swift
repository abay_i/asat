//
//  MenuViewController.swift
//  Asat
//
//  Created by Александр on 13.08.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import Kingfisher

class MenuViewController: UITableViewController, IndicatorInfoProvider {
    
    var itemInfo: IndicatorInfo = ""
    
    var menu: [RestoransMenu] = []
    
    var restoran: Restoran!
    
    
    init(itemInfo: IndicatorInfo) {
        self.itemInfo = itemInfo
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        
        BasketList.initBasket()
        
        tableView.register(UINib(nibName: "RestoranMenuCell", bundle: nil), forCellReuseIdentifier: "cell")
        tableView.backgroundColor = UIColor.groupTableViewBackground
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menu.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! RestoranMenuTableViewCell
        
        cell.nameLabel.text = menu[indexPath.row].name
        cell.priceLabel.text = menu[indexPath.row].price!.description + " тг"
        if let photo = menu[indexPath.row].photo {
            cell.foodImageView.kf.setImage(with: URL(string: photo), options: [.transition(.fade(1.2))])
        }
        cell.indexPath = indexPath
        cell.delegate = self
        
        cell.counterView.value = 0
        cell.counterView.countLabel.text = cell.counterView.value.description
        cell.priceLabel.text = cell.priceLabel.text!
        
        if let foodInBascet = BasketList.findIndex(by: menu[indexPath.row].id!) {
            cell.counterView.value = foodInBascet.basket.count
            cell.counterView.countLabel.text = foodInBascet.basket.count.description
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }

}

extension MenuViewController: RestoranMenuTableViewCellProtocol {
    func counterWasChanged(indexPath: IndexPath, count: Int) {
        BasketList.changeOrAdd(restoran: restoran, food: menu[indexPath.row], count: count)
        tableView.reloadRows(at: [indexPath], with: .none)
    }
}

