//
//  MenuTabsViewController.swift
//  Asat
//
//  Created by Александр on 13.08.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class MenuTabsViewController: ButtonBarPagerTabStripViewController {
    
    var categories: [MenuCategory] = []
    var menu: [RestoransMenu] = []
    var index: Int = 1
    var restoran: Restoran!

    override func viewDidLoad() {
        

        settings.style.buttonBarItemLeftRightMargin = 8
        settings.style.buttonBarMinimumLineSpacing = 8
        settings.style.buttonBarItemBackgroundColor = UIColor.white
        settings.style.buttonBarItemTitleColor = .lightGray
        settings.style.buttonBarHeight = 60
                
        super.viewDidLoad()

        buttonBarView.backgroundColor = .white
        buttonBarView.selectedBar.backgroundColor = UIColor(hex: "#039be5")
    
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.moveToViewController(at: strongSelf.index, animated: false)
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.reloadPagerTabStripView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        
        var controllers: [MenuViewController] = []
        
        for i in 0..<categories.count {
            let controller = MenuViewController(itemInfo: IndicatorInfo(title: categories[i].name!))
            controller.menu = menu.filter({$0.category_id == categories[i].id})
            controller.restoran = restoran
            controllers.append(controller)
        }
        
        return controllers
    }
    

}
