//
//  MenuParentViewController.swift
//  Asat
//
//  Created by Александр on 13.08.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit

class MenuParentViewController: ClientViewController {

    var categories: [MenuCategory] = []
    var menu: [RestoransMenu] = []
    var index: Int = 0
    var restoran: Restoran!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Меню"
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "MenuTabsSegue" {
            let controller = segue.destination as! MenuTabsViewController
            controller.categories = categories
            controller.menu = menu
            controller.index = index            
            controller.restoran = restoran
        }
    }

}
