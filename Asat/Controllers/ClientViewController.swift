//
//  ClientViewController.swift
//  Asat
//
//  Created by Александр on 17.08.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit

class ClientViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.rightBarButtonItem = UIBarButtonItem.init(image: UIImage(named:"basket"), style: .plain, target: self, action: #selector(moveToBasket))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func moveToBasket() {
        let controller = storyboard?.instantiateViewController(withIdentifier: "BasketController") as! BasketViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    

}
