//
//  ChatViewController.swift
//  Asat
//
//  Created by Александр on 13.11.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit

class ChatViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    
    var chat: JivoSdk!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.isTranslucent = false
        
        title = "Чат"
        
        chat = JivoSdk(webView, "ru")
        chat.delegate = self
        chat.prepare()
        setSideMenuButton()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        chat.start()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        chat.stop()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}

extension ChatViewController: JivoDelegate {
    func onEvent(_ name: String!, _ data: String!) {
        NSLog("event: %@, data: %@", name, data)
    }
}
