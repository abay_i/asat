//
//  RestoransFilterViewController.swift
//  Asat
//
//  Created by Александр on 02.10.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit
import ObjectMapper

class RestoransFilterViewController: UIViewController {
    
    var data: [[Any]] = []
    var sectionsTitle = ["Выберите параметры заведения", "Выберите кухни"]
    
    static var params: [Int: [Int]] = [:]
    var params: [Int: [Int]] = [:]

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var saveButton: UIButton!
    
    var restoranListFilterDelegate: RestoranListViewControllerFilter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Отмена", style: .plain, target: self, action: #selector(dissmissToRestoransList))
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Очистить", style: .plain, target: self, action: #selector(clearParams))
        
        title = "Фильтр"

        data.append(["Доставка", "Оплата онлаин", "Еда с кешбеком"])
        data.append( Mapper<Kitchen>().mapArray(JSONArray: API.dics?[2]["values"].arrayObject as! [[String : Any]]))
        
        if RestoransFilterViewController.params.isEmpty {
            clearParams()
        }
        
        self.params = RestoransFilterViewController.params
        
        tableView.separatorStyle = .none
        tableView.dataSource = self
        tableView.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func saveButtonAction(_ sender: UIButton) {
        RestoransFilterViewController.params = self.params
        restoranListFilterDelegate?.filter()
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func dissmissToRestoransList() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func clearParams() {
        for index in data.enumerated() {
            RestoransFilterViewController.params[index.offset] = []
            params[index.offset] = []
        }
        tableView.reloadData()
    }

}

extension RestoransFilterViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        switch indexPath.section {
        case 0:
            cell.textLabel?.text = data[indexPath.section][indexPath.row] as? String
        case 1:
            cell.textLabel?.text = (data[indexPath.section][indexPath.row] as! Kitchen).name
        default:
            return cell
        }
        
        cell.accessoryType = .none
        
        if self.params[indexPath.section] != nil {
            switch indexPath.section {
            case 0:
                cell.accessoryType = self.params[indexPath.section]!.contains(indexPath.row) ? .checkmark : .none
            case 1:
                cell.accessoryType = self.params[indexPath.section]!.contains((data[indexPath.section][indexPath.row] as! Kitchen).id!) ? .checkmark : .none
            default:
                break
            }
            
        }
        
        return cell
    }
}

extension RestoransFilterViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionsTitle[section]
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.section {
        case 0:
            if self.params[indexPath.section]!.contains(indexPath.row) {
                self.params[indexPath.section]?.removeObject(indexPath.row)
            } else {
                self.params[indexPath.section]?.append(indexPath.row)
            }
        case 1:
            if self.params[indexPath.section]!.contains((data[indexPath.section][indexPath.row] as! Kitchen).id!) {
                self.params[indexPath.section]?.removeObject((data[indexPath.section][indexPath.row] as! Kitchen).id!)
            } else {
                self.params[indexPath.section]?.append((data[indexPath.section][indexPath.row] as! Kitchen).id!)
            }
        default:
            break
        }
        
        
        tableView.reloadData()
    }
}
