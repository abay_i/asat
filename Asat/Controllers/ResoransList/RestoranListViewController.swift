//
//  RestoranListViewController.swift
//  Emenu
//
//  Created by Александр on 18.06.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import Kingfisher
import ObjectMapper

protocol RestoranListViewControllerFilter {
    func filter()
}

class RestoranListViewController: ClientViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var restorans: [Restoran] = []
    var filteredRestorans: [Restoran] = []
    

    @IBOutlet weak var filterButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        //tableView.register(UINib(nibName: "RestoranListCell", bundle: nil), forCellReuseIdentifier: "restoranListCell")
        self.setSideMenuButton()
        navigationItem.title = "Рестораны"
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard restorans.isEmpty else {
            return
        }
        
        let hud = self.showProgressHud()
        API.getRestorans(params: ["expand": "logo,info,locations"], compliteHandler: {(data, error, code) -> Void in
            if let restorans = data {
                self.restorans = restorans
                self.filterRestorans()
            } else {
                DispatchQueue.main.async { [weak self] in
                    self?.alertValidation(message: error!, title: "Не удалось загрузить")
                }
                print(error!, code!) // Сюда алерт с ошибкой
            }
            
            DispatchQueue.main.async { [weak self] in
                self?.hideProgressHud(hud)
            }
        })
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    @IBAction func filterButtonAction(_ sender: UIButton) {
        let controller = storyboard?.instantiateViewController(withIdentifier: "RestoransFilterViewController") as! RestoransFilterViewController
        controller.restoranListFilterDelegate = self
        let nav = UINavigationController(rootViewController: controller)
        self.present(nav, animated: true)
    }
    
    func filterRestorans() {
        self.filteredRestorans = self.restorans
        print(RestoransFilterViewController.params)
        if RestoransFilterViewController.params.isEmpty {
            tableView.reloadData()
            return
        }
        
        if !RestoransFilterViewController.params[0]!.isEmpty {
            for index in RestoransFilterViewController.params[0]! {
                switch index {
                case 0:
                    filteredRestorans = filteredRestorans.filter{ $0.pickup == true }
                case 1:
                    filteredRestorans = filteredRestorans.filter{ $0.online_payment == true }
                case 2:
                    filteredRestorans = filteredRestorans.filter{ $0.cash_back != nil }
                default:
                    continue
                }
            }
        }
        
        if !RestoransFilterViewController.params[1]!.isEmpty {
            let ids = RestoransFilterViewController.params[1]!
            filteredRestorans = filteredRestorans.filter({ (restoran) -> Bool in
                for id in ids {
                    let isContain = restoran.kitchens.contains(id)
                    if isContain {
                        return true
                    }
                    
                    continue
                }
                
                return false
            })
        }
        
        tableView.reloadData()
        
    }
    

}


extension RestoranListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredRestorans.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "restoransListCell", for: indexPath) as! RestoransListCell
    
        let restoran = filteredRestorans[indexPath.row]
        cell.restoranTitleLabel.text = restoran.name
        cell.minPriceLabel.text = restoran.min_price_description
        
        if let workStart = restoran.work_start, let workEnd = restoran.work_end {
            cell.workTimeLabel.text = "\(workStart)-\(workEnd)"
        }
        
        if let cashBack = restoran.cash_back {
            cell.cashbackLabel.text = "Бонус: \(cashBack)%"
        }
        
        if let logo = restoran.logo {
            cell.restoranImageView.kf.setImage(with: URL(string: logo), options: [.transition(.fade(0.5))])
        }
        
        if let onlinePayment = restoran.online_payment, onlinePayment {
            cell.cardYesNoImageView.image = UIImage(named: "yes")
        } else {
            cell.cardYesNoImageView.image = UIImage(named: "no")
        }
        
        if let pickup = restoran.pickup, pickup {
            cell.deliveryYesNoImageView.image = UIImage(named: "yes")
        } else {
            cell.deliveryYesNoImageView.image = UIImage(named: "no")
        }
        

        if let locations = restoran.locations {
            if locations.count > 0 {
                
                if let time = locations[0].delivery_time {
                    cell.deliveryTimeLabel.text = "\(time) мин"
                }
                if let price = locations[0].price {
                    cell.deliveryPriceLabel.text = "\(price) тг"
                }
            }
        }
        
        if let rating = restoran.rating {
            cell.ratingView.rating = Double(rating)
        }        
        
        
        cell.layer.shadowOffset = CGSize(width: 0, height: 3)
        cell.layer.shadowRadius = 3
        cell.layer.shadowOpacity = 0.3
        
        return cell
    }
    
}


extension RestoranListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let id = restorans[indexPath.row].id
        let controller = storyboard?.instantiateViewController(withIdentifier: "RestoranController") as! RestoranViewController
        controller.restoranId = id
        controller.restoran = restorans[indexPath.row]
        
        let backItem = UIBarButtonItem()
        backItem.title = nil
        navigationItem.backBarButtonItem = backItem
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

extension RestoranListViewController: RestoranListViewControllerFilter {
    func filter() {
        self.filterRestorans()
    }
}
