//
//  ProfileViewController.swift
//  Asat
//
//  Created by Александр on 24.08.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import Kingfisher

class ProfileViewController: UIViewController {
    
    @IBOutlet weak var avatarImageView: UIImageView!
    
    @IBOutlet weak var fioSkyTextFiled: SkyFloatingLabelTextField!
    @IBOutlet weak var phoneSkyTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var emailSkyTextField: SkyFloatingLabelTextField!
    
    @IBOutlet weak var saveButton: UIButton!
    var imagePicker = UIImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()

        imagePicker.delegate = self
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(setImage))
        tap.numberOfTapsRequired = 1
        
        avatarImageView.layer.cornerRadius = avatarImageView.frame.width / 2
        avatarImageView.addGestureRecognizer(tap)
        avatarImageView.isUserInteractionEnabled = true
        avatarImageView.clipsToBounds = true
        
        hideKeyboardWhenTappedAround()
        setSideMenuButton()
        title = "Профиль"
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let app = (UIApplication.shared.delegate as! AppDelegate)
        
        if let user = app.user {
            fioSkyTextFiled.text = user.fio
            phoneSkyTextField.text = user.phone
            emailSkyTextField.text = user.email
            if let photo = user.photo {
                 avatarImageView.kf.setImage(with: URL(string: photo), options: [.transition(.fade(1.2))])
            }
        }
    }
    
    @objc func setImage() {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")
            
            
            imagePicker.sourceType = .savedPhotosAlbum;
            imagePicker.allowsEditing = false
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }

    @IBAction func saveButtonAction(_ sender: UIButton) {
        let hud = showProgressHud()
        let params: [String: Any] = [
            "fio": fioSkyTextFiled.text!,
            "phone": phoneSkyTextField.text!,
            "email": emailSkyTextField.text!
        ]
        API.changeUser(params: params, compliteHandler: {(data, error, code) -> Void in
            if let _ = data {
                (UIApplication.shared.delegate as! AppDelegate).user?.fio = self.fioSkyTextFiled.text!
                (UIApplication.shared.delegate as! AppDelegate).user?.phone = self.phoneSkyTextField.text!
                (UIApplication.shared.delegate as! AppDelegate).user?.email = self.emailSkyTextField.text!
                self.alertValidation(message: "Настройки сохранены", title: "Готова!")
                (self.sideMenuController?.leftViewController as! SideMenuViewController).reload()
            } else {
                self.alertValidation(message: "Не удалось сохранить данные", title: "Ошибка")
            }
            self.hideProgressHud(hud)
        })
    }
    
}


extension ProfileViewController: UIImagePickerControllerDelegate {
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!){
        dismiss(animated: true, completion: { () -> Void in
            self.avatarImageView.image = image
        })
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.avatarImageView.contentMode = .scaleAspectFill
            self.avatarImageView.image = pickedImage
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
}

extension ProfileViewController: UINavigationControllerDelegate {}
