//
//  RegistrationViewController.swift
//  Asat
//
//  Created by Александр on 09.07.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit

class RegistrationViewController: ViewController {
    
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    
    @IBOutlet weak var registrationButton: UIButton!

    @IBOutlet weak var avatarImageView: UIImageView!
    
    var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registrationButton.setTitle("Зарегистрироваться".uppercased(), for: .normal)
        avatarImageView.layer.cornerRadius = avatarImageView.frame.width / 2
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(setImage))
        tap.numberOfTapsRequired = 1
        
        avatarImageView.addGestureRecognizer(tap)
        avatarImageView.isUserInteractionEnabled = true
        avatarImageView.clipsToBounds = true
        
        emailTextField.delegate = self
        passwordTextField.delegate = self
        phoneTextField.delegate = self
        nameTextField.delegate = self
        
        self.hideKeyboardWhenTappedAround()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }

    @IBAction func registrationButtonAction(_ sender: UIButton) {
        
        // Раскометрировать когда будет юзер
        
        let hud = self.showProgressHud()
        
        guard !emailTextField.text!.isEmpty else {
            alertValidation(message: "Необходимо заполнить «Email»", title: "Ошибка")
            return
        }
        
        guard !passwordTextField.text!.isEmpty else {
            alertValidation(message: "Необходимо заполнить «Password»", title: "Ошибка")
            return
        }
        
        guard !phoneTextField.text!.isEmpty else {
            alertValidation(message: "Необходимо заполнить «Телефон»", title: "Ошибка")
            return
        }
        
//        let params: [String: Any] = [
//            "phone": phoneTextField.text!,
//            "email": emailTextField.text!,
//            "password": passwordTextField.text!,
//            "fio": nameTextField.text!
//        ]
        
        API.registration(email: emailTextField.text!, password: passwordTextField.text!, phone: phoneTextField.text!, fio: nameTextField.text!, compliteHandler: {(data, errors, code) -> Void in
            
            PushManager.updateFCMToken()
            
            if let token = data["token"].string {
                UserDefaults.token = token
                self.present(FirstViewController(), animated: true, completion: nil)
            } else {
                print(data)
                self.alertValidation(message: "Не удалось зарегистрировать", title: "Ошибка")
            }
            
            self.hideProgressHud(hud)
            
        })
        
        
    }
    
    @objc func setImage() {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")
            
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum;
            imagePicker.allowsEditing = false
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    
}

extension RegistrationViewController: UIImagePickerControllerDelegate {
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!){
        self.dismiss(animated: true, completion: { () -> Void in
             self.avatarImageView.image = image
        })
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.avatarImageView.contentMode = .scaleAspectFill
            self.avatarImageView.image = pickedImage
        }
        
        dismiss(animated: true, completion: nil)
    }
}

extension RegistrationViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
}

extension RegistrationViewController: UINavigationControllerDelegate {
    
}
