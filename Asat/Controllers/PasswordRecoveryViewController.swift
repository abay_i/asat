//
//  PasswordRecoveryViewController.swift
//  Asat
//
//  Created by Abay on 7/6/18.
//  Copyright © 2018 Александр. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class PasswordRecoveryViewController: ViewController {

    @IBOutlet weak var phoneTextView: SkyFloatingLabelTextField!
    
    @IBAction func nextAction(_ sender: UIButton) {
                
        guard !phoneTextView.text!.isEmpty else {
            alertValidation(message: "Необходимо заполнить «Телефон»", title: "Ошибка")
            return
        }
        
        let hud = self.showProgressHud()
        
        
        API.recoverPhone(phone: phoneTextView.text!, compliteHandler: { [weak self] (data, errors, code) -> Void in
            if let error = errors {
                print(error)
                self?.alertValidation(message: error, title: "Не удалось восстановить пароль")
            } else {
                if let controller = self?.storyboard?.instantiateViewController(withIdentifier: "ConfirmPasswordRecoveryViewController") as? ConfirmPasswordRecoveryViewController {
                    controller.phoneNumber = self?.phoneTextView.text!
                    controller.userId = data["user_id"].stringValue
                    self?.navigationController?.pushViewController(controller, animated: true)
                }
            }
            
            self?.hideProgressHud(hud)
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        phoneTextView.delegate = self

        self.hideKeyboardWhenTappedAround()
    }

}

extension PasswordRecoveryViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
}
