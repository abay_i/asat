//
//  DiscountViewController.swift
//  Asat
//
//  Created by Александр on 12.11.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit

class DiscountViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var discouns: [Discount] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        setSideMenuButton()
        
        title = "Акции"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let hud = self.showProgressHud()
        API.getDiscounts(compliteHandler: {(data, error, code) -> Void in
            if let discounts = data {
                self.discouns = discounts
                self.tableView.reloadData()
            } else {
                self.alertValidation(message: "Не удалось загрузить данные", title: "Ошибка")
            }
            
            self.hideProgressHud(hud)
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}

extension DiscountViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return discouns.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! DiscountTableViewCell
        
        cell.selectionStyle = .none
        cell.titleLabel.text = discouns[indexPath.row].name
        cell.descriptionLabel.text = discouns[indexPath.row].description
        if let photo = discouns[indexPath.row].photo{
            cell.DiscoutImageView.kf.setImage(with: URL(string: photo), options: [.transition(.fade(1.2))])
        }
        
        return cell
    }
}

extension DiscountViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let hud = self.showProgressHud()
        API.getRestoran(id: discouns[indexPath.row].restaurant_id!, params: ["expand": "info"], compliteHandler: {(data, error, code) -> Void in
            if let restoran = data {
                let controller = self.storyboard?.instantiateViewController(withIdentifier: "RestoranController") as! RestoranViewController
                controller.restoranId = restoran.id!
                controller.restoran = restoran
                self.navigationController?.pushViewController(controller, animated: true)
            } else {
                self.alertValidation(message: "Не удалось загрузить ресторан", title: "Ошибка!")
            }
            self.hideProgressHud(hud)
        })
        
    }
}
