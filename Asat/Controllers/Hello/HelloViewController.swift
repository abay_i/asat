//
//  HelloViewController.swift
//  Asat
//
//  Created by Александр on 09.07.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit

class HelloViewController: ViewController {
    
    @IBOutlet weak var courierRegisterButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftBarButtonItem = nil
        
        courierRegisterButton.setAttributedTitle(String.getWhiteBlueAttrString(white: "Хочу стать ", blue: "курьером"), for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.navigationController?.isNavigationBarHidden = true
        self.setTransculentNavigationBar()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        (appDelegate.window as! WallpaperWindow).wallpaper = #imageLiteral(resourceName: "BackgroundHello")       
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        //self.navigationController?.isNavigationBarHidden = false
        //
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
