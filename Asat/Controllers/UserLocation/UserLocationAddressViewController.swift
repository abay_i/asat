//
//  UserLocationAddressViewController.swift
//  Asat
//
//  Created by Александр on 19.09.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit

class UserLocationAddressViewController: ViewController {
    
    var town_id: Int!
    var address: String!
    var isRegistration = false
    
    @IBOutlet weak var addressSkyTextField: UITextField!
    @IBOutlet weak var finishLocationButton: UIButton!

    @IBOutlet weak var backgroundImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addressSkyTextField.delegate = self

        // Do any additional setup after loading the view.
    }

    override func internalViewDidLoad() {
        self.addressSkyTextField.text = address
        hideKeyboardWhenTappedAround()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func finishLocationButtonAction(_ sender: UIButton) {
        if !addressSkyTextField.text!.isEmpty {
            
            let userInfo = UserInfo()
            userInfo.address = addressSkyTextField.text!
            userInfo.city_id = town_id
            UserDefaults.userInfo = userInfo
            
//            if isRegistration {
//                let registration = storyboard?.instantiateViewController(withIdentifier: "RegistrationController") as! RegistrationViewController
//                self.navigationController?.pushViewController(registration, animated: true)
//                return
//            }
            
            self.subviewsHidden(hidden: true)
            self.present(MainMenuViewController(), animated: true, completion: nil)
           // backgroundImage.image = #imageLiteral(resourceName: "BackgroundLocation")
        }
    }
}


extension UserLocationAddressViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
}

