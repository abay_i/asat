//
//  UserLocationMenuViewController.swift
//  Asat
//
//  Created by Александр on 09.08.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit

class UserLocationMenuViewController: ViewController {

    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var backgroundImage: UIImageView!
    
    var setWindow = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let address = UserDefaults.userInfo.address {
            self.addressLabel.text = address
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        (appDelegate.window as! WallpaperWindow).wallpaper = #imageLiteral(resourceName: "BackgroundLocation")
        
        if !setWindow {
            backgroundImage.image = #imageLiteral(resourceName: "BackgroundLocation")
            //let buttonExit = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
            let barItem = UIBarButtonItem(title: "Закрыть", style: .plain, target: self, action: #selector(self.exit))
            self.navigationItem.leftBarButtonItem = barItem
        }
    }
    
    @objc func exit() {
        self.dismiss(animated: true, completion: {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            (appDelegate.window as! WallpaperWindow).wallpaper = #imageLiteral(resourceName: "BackgroundHello")
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        (appDelegate.window as! WallpaperWindow).wallpaper = #imageLiteral(resourceName: "BackgroundHello")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
