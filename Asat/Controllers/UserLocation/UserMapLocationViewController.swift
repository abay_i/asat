//
//  UserMapLocationViewController.swift
//  Asat
//
//  Created by Александр on 01.10.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit
import MapKit
import GoogleMaps

class UserMapLocationViewController: ViewController  {

    //MARK: - IBoutlets
    
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var containerMapView: UIView!
    
    //MARK: - Variables
    
    var location: CLLocationCoordinate2D?
    var mapView: GMSMapView!
    var marker: GMSMarker?
    var geocoder : GMSGeocoder = GMSGeocoder()
    var isInitializedGoogleMap: Bool = false
    var cameraPosition: GMSCameraPosition!
    var address: String = ""
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addGoogleMap()
        self.title = ""//"Определение геолокации"
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        useLocation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        subviewsHidden(hidden: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Google Map
    
    private func addGoogleMap() {
        cameraPosition = GMSCameraPosition.camera(withLatitude: 0,
                                                  longitude:0, zoom:1)
        
        let frame = self.view.frame
        
        mapView = GMSMapView.map(withFrame: frame, camera:cameraPosition)
        mapView.mapType = .hybrid
        mapView.accessibilityElementsHidden = false
        mapView.delegate = self
        containerMapView.addSubview(mapView)
        isInitializedGoogleMap = true
        self.view.bringSubview(toFront: btnDone)
    }
    
    public func coordinateToAddress(coordinate : CLLocationCoordinate2D, result : @escaping (String) -> Void) {
        geocoder.reverseGeocodeCoordinate(coordinate) { (response, error) in
            if let address = response?.firstResult(), let lines = address.lines, lines.count > 0 {
                let findedAddress = lines[0]
                result(findedAddress)
            }
        }
    }
    
    private func animateCameraToLocation(coordinate:CLLocationCoordinate2D) {
        if (isInitializedGoogleMap) {
            cameraPosition = GMSCameraPosition.camera(withLatitude: coordinate.latitude,
                                                      longitude:coordinate.longitude, zoom:16)
            mapView.animate(toLocation: coordinate)
            CATransaction.begin()
            CATransaction.setValue(Int(2), forKey: kCATransactionAnimationDuration)
            mapView.animate(to: cameraPosition)
            CATransaction.commit()
        }
    }
    
    private func useLocation() {
        SLocationManager.sharedInstance.getCurrentCoordinate(recievedCoordinate: { (coordinate) in
            self.animateCameraToLocation(coordinate: coordinate)
            self.marker = GMSMarker()
            self.marker?.map = self.mapView
            self.marker?.position = coordinate
            self.location = coordinate
        }) { (error) in
            UIAlertController.showError(viewController: self, message: error?.localizedDescription ?? "")
        }
    }

    //MARK: - IBActions
    
    @IBAction func btnDone_Click(_ sender: Any) {
        if let loc = location {
            let hud = self.showProgressHud()
            API.getGeocodeId(lat: loc.latitude, lon: loc.longitude, onComplete: { (id) in
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "locationAddressIdentity") as! UserLocationAddressViewController
                vc.town_id = id
                vc.address = self.address
                self.navigationController?.pushViewController(vc, animated: true)
                self.hideProgressHud(hud)
            }, onError: { (message) in
                print(message ?? "")
                UIAlertController.showError(viewController: self, message: message ?? "")
                self.hideProgressHud(hud)
            })
        }
    }
    
    
    
    /*
    func getAddress(location: CLLocation) {
        showProgressHud()
        API.getAddressByLocation(lat: location.coordinate.latitude, lon: location.coordinate.longitude, compliteHandler: { (data, error, code) -> Void in
            if let address = data {
                self.userAdsress = address["address"].string
                self.alertValidation(message: "Your Address is \(self.userAdsress!)", title: "Complite")
            }
            self.hideProgressHud()
        })
    }*/

}

extension UserMapLocationViewController: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        //Draw marker
        marker?.position = mapView.camera.target
        location = marker?.position
        if let position = marker?.position {
            coordinateToAddress(coordinate: position) { (text) in
                self.address = text
                //self.delivery.address = text
            }
        }
    }
    
}
