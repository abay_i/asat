//
//  UserLocationTownViewController.swift
//  Asat
//
//  Created by Александр on 09.08.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit
import ObjectMapper

class UserLocationTownViewController: ViewController {
    
    @IBOutlet weak var searchTownTextView: UITextField!
    @IBOutlet weak var searchTableView: UITableView!
    @IBOutlet weak var nextButton: UIButton!
    
    @IBOutlet weak var backgroundImage: UIImageView!
    var town_id: Int?
    
    var towns: [City] = []
    var searchTowns: [City] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchTownTextView.delegate = self
        
        searchTableView.dataSource = self
        searchTableView.delegate = self
        
        searchTownTextView.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
       // backgroundImage.image = #imageLiteral(resourceName: "BackgroundLocation")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.subviewsHidden(hidden: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard API.dics == nil else {
            self.towns = Mapper<City>().mapArray(JSONArray: API.dics?[0]["values"].arrayObject as! [[String : Any]])
            return
        }
        
        let hud = self.showProgressHud()
        API.getDics(compliteHandler: { (data, error, code) -> Void in
            
            guard error == nil && code == 200 else {
                self.alertValidation(message: "Не удалось выполнить запрос", title: "Ошибка!")
                self.hideProgressHud(hud)
                return
            }
            
            if let json = data {
                self.towns = Mapper<City>().mapArray(JSONArray: json[0]["values"].arrayObject as! [[String : Any]])
            }
            
            self.hideProgressHud(hud)
        })
    }
    
    @objc func textFieldDidChange(textField: UITextField) {
        let text = textField.text!
        
        searchTowns = towns.filter({ (town: City) -> Bool in
            let match = town.name?.lowercased().range(of: text.lowercased(), options: NSString.CompareOptions.caseInsensitive)
            
            if text == "" {
                return false
            }
            
            return match != nil
        })
        
        //searchTowns.sort()
        searchTableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toAddressController" {            
            let controller = segue.destination as! UserLocationAddressViewController
            controller.town_id = town_id!
        }
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "toAddressController" {
            return town_id != nil
        }
        
        return true
    }
    
}


extension UserLocationTownViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        return true
    }
}


extension UserLocationTownViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchTowns.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        cell.textLabel?.text = searchTowns[indexPath.row].name
        
        return cell
    }

}

extension UserLocationTownViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        searchTownTextView.text = searchTowns[indexPath.row].name
        town_id = searchTowns[indexPath.row].id
        searchTowns = []
        tableView.reloadData()
        searchTownTextView.endEditing(true)
    }
}
