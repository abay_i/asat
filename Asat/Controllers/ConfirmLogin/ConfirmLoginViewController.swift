//
//  ConfirmLoginViewController.swift
//  Asat
//
//  Created by Александр on 06.08.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit

class ConfirmLoginViewController: ViewController {

    @IBOutlet weak var repeatConfirmButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        repeatConfirmButton.setAttributedTitle(String.getWhiteBlueAttrString(white: "Не получили SMS? ", blue: "Повторить"), for: .normal)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
