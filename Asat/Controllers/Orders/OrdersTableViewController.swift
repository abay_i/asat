//
//  OrdersTableViewController.swift
//  Asat
//
//  Created by Александр on 27.08.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit

class OrdersTableViewController: UITableViewController {
    
    var orders: [Order] = []
    
    let fullDateFormatter = DateFormatter()
    let dateFormatter = DateFormatter()
    let timeDateFormatter = DateFormatter()

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.hideKeyboardWhenTappedAround()

        self.title = "Заказы"
        self.tableView.backgroundColor = UIColor.groupTableViewBackground
        self.setSideMenuButton()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        fullDateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
        dateFormatter.dateFormat = "dd-MM-yyyy"
        timeDateFormatter.dateFormat =  "hh:mm"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadOrders()
    }
    
    private func loadOrders() {
        let hud = self.showProgressHud()
        API.getOrders(compliteHandler: {(data, error, code) -> Void in
            if let orders = data {
                self.orders = orders
                self.tableView.reloadData()
            } else {
                self.alertValidation(message: error!, title: "Wrong!")
            }
            self.hideProgressHud(hud)
        })
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.orders.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! OrdersTableViewCell
        
        let order = orders[indexPath.row]
        
        cell.order = order
        cell.nameLabel.text = order.restaurant?.name
        cell.priceLabel.text = "Сумма \((order.price?.description ?? "")) тг"
        cell.statusLabel.text = order.statusDesc
        cell.selectionStyle = .none
        
        if let id = order.id {
            cell.idLabel.text = "№\(id)"
        }
        
        if let rating = order.restaurant?.rating {
            cell.ratingView.rating = Double(rating)
        }
        
        if let ts = order.ts {
            if let date = fullDateFormatter.date(from: ts) {
                cell.tsDateLabel.text = dateFormatter.string(from: date)
                cell.tsTimeLabel.text = timeDateFormatter.string(from: date)
            }
        }
        
        cell.payButton.isHidden = true
        cell.payButton.isUserInteractionEnabled = true
        cell.cancelButton.isHidden = true
        cell.cancelButton.setTitle("Отмена", for: .normal)
        if let status = order.status {
            switch (status) {
            case 0:
                cell.statusImageView.image = UIImage(named: "client_order_status_new")
                cell.cancelButton.isHidden = false
                
                if let paid = order.paid, paid {
                    cell.payButton.isHidden = false
                    cell.payButton.isUserInteractionEnabled = false
                    cell.payButton.setTitle("Оплачен", for: .normal)
                } else {                    
                    if let paymentType = order.payment_type {
                        if paymentType == 1  || paymentType == 3 {
                            cell.payButton.isHidden = false
                        }
                    }
                }
            case 1:
                cell.statusImageView.image = UIImage(named: "client_order_in_process")
                cell.cancelButton.isHidden = true
            case 2:
                cell.statusImageView.image = UIImage(named: "client_order_in_transit")
                cell.cancelButton.isHidden = true
            case 3:
                cell.statusImageView.image = UIImage(named: "client_order_delivered")
                cell.cancelButton.isHidden = true
            case 4:
                cell.statusImageView.image = UIImage(named: "client_order_cancelled")
                cell.cancelButton.isHidden = false
                cell.cancelButton.setTitle("Причина", for: .normal)
            default:
                break
            }
        }
        
        
        
        if let logo = orders[indexPath.row].restaurant?.info?.logo {
            cell.logoImageView.kf.setImage(with: URL(string: logo), options: [.transition(.fade(0.5))])
        }
        
//        if let photo = orders[indexPath.row].restaurant?.info?.photo {
//            cell.backImageView.kf.setImage(with: URL(string: photo), options: [.transition(.fade(1.2))])
//        }
        
        cell.layer.shadowOffset = CGSize(width: 0, height: 3)
        cell.layer.shadowRadius = 3
        cell.layer.shadowOpacity = 0.3

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let viewOrderController = storyboard?.instantiateViewController(withIdentifier: "OrderItemsViewController") as! OrderItemsViewController
        viewOrderController.order = orders[indexPath.row]
        self.navigationController?.pushViewController(viewOrderController, animated: true)
        
    }
    
}
