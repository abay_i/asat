//
//  OrderItemsTableViewController.swift
//  Asat
//
//  Created by Abay on 6/24/18.
//  Copyright © 2018 Александр. All rights reserved.
//

import UIKit

class OrderItemsViewController: UIViewController {

    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var totalSumLabel: UILabel!
    
    var order: Order!
    var items: [OrderItem] {
        get {
            return order.getUniqItems()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let title = order.restaurant?.name {
            self.title = title
        }
        
        tableView.dataSource = self
        totalSumLabel.text = "Общая сумма заказа \(order.price ?? 0) тг"
    }
}

extension OrderItemsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderItemCell", for: indexPath) as! OrderItemTableViewCell
        let item = items[indexPath.row]
        cell.nameLabel?.text = item.product?.name
        
        if let price = item.price {
            cell.priceLabel.text = "Сумма \(price) тг"
        }
        
        cell.countLabel.text = "Колличество: \(item.count)"
        
        if let photo = item.product?.photo {
            cell.logoImageView?.kf.setImage(with: URL(string: photo), options: [.transition(.fade(0.5))])
        }
        return cell
    }
}

