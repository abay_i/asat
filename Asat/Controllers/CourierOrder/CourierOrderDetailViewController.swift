//
//  CourierOrderDetailViewController.swift
//  Asat
//
//  Created by Александр on 23.10.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit

class CourierOrderDetailViewController: UIViewController {
    
    @IBOutlet weak var fromLabel: UILabel!
    @IBOutlet weak var toLabel: UILabel!
    @IBOutlet weak var deliveryCostLabel: UILabel!
    @IBOutlet weak var orderView: UIView!
    @IBOutlet weak var sendOrderButton: UIButton!
    
    var order: Order!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        orderView.setShadow()
        
        fromLabel.text = order.restaurant?.info?.address
        toLabel.text = order.address
        deliveryCostLabel.text = 550.description // Времмено! Не знаю откуда это брать!
        
        setup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setup() {
        self.sendOrderButton.setTitle(self.setupSendOrderButtonTitle(), for: .normal)
    }
    

    func setupSendOrderButtonTitle() -> String {
        let titles: [Int : String] = [
            1: "Принять заказ",
            2: "Доставлен",
        ]
        
        guard titles[order.status!] != nil else {
            return ""
        }
        
        return titles[order.status!]!
        
    }
    
    func sendOrder(status: Int) {
        let params: [String: Any] = [
            "status": status
        ]
        let hud = self.showProgressHud()
        API.changeOrder(id: order.id!, params: params, compliteHandler: { (data, error, code) -> Void in
            if let order = data {
                self.order.status = order.status
                if status == 3 {
                    self.navigationController?.popViewController(animated: true)
                    return
                }
                self.setup()
            } else {
                self.alertValidation(message: "Не удалось оьновить заказ", title: "Ошибка")
            }
            
            self.hideProgressHud(hud)
        })
    }
    
    @IBAction func sendOrderButtonAction(_ sender: UIButton) {
        var status = order.status
        
        if status == 1 {
            status = 2
        } else if status == 2 {
            status = 3
        }
        
        sendOrder(status: status!)
    }
    
}
