//
//  CourierHistiryViewController.swift
//  Asat
//
//  Created by Александр on 26.12.2017.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit

class CourierHistoryViewController: OrdersTableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 80
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! OrdersTableViewCell
        
        cell.nameLabel.text = "Откуда: \(orders[indexPath.row].restaurant?.info?.address ?? "") \n\nКуда: \(orders[indexPath.row].address ?? "")"
        cell.priceLabel.text = orders[indexPath.row].price!.description + " тг"
//        cell.statusButton.setTitle("\(orders[indexPath.row].statusDesc)", for: .normal)
        cell.selectionStyle = .none
        
        
        
        return cell
    }
    


}
