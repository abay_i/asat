//
//  CourierOrdersViewController.swift
//  Asat
//
//  Created by Александр on 10.10.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit

class CourierOrdersViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var orders: [Order] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.separatorStyle = .none
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let hud = self.showProgressHud()
        API.getOrders(urlString: "/orders?&sort=-ts&expand=items.product,restaurant.info", compliteHandler: { (data, error, code) -> Void in
            if let orders = data {
                self.orders = orders
                self.tableView.reloadData()
            }
            self.hideProgressHud(hud)
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension CourierOrdersViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CourierOrderTableViewCell
        
        cell.fromValueLabel.text = orders[indexPath.row].restaurant?.info?.address
        cell.toValueLabel.text = orders[indexPath.row].address
        cell.selectionStyle = .none
        
        return cell
    }
    
}

extension CourierOrdersViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller = storyboard?.instantiateViewController(withIdentifier: "CourierOrderDetailViewController") as! CourierOrderDetailViewController
        controller.order = orders[indexPath.row]
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

